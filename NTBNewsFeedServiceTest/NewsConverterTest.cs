﻿using System.IO;
using NTB.NewsFeed.Utilities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Xml.XPath;

namespace NTBNewsFeedServiceTest
{
    
    
    /// <summary>
    ///This is a test class for NewsConverterTest and is intended
    ///to contain all NewsConverterTest Unit Tests
    ///</summary>
    [TestClass]
    public class NewsConverterTest
    {
        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext { get; set; }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for GetDocumentTitle
        ///</summary>
        [TestMethod]
        [DeploymentItem("NTBNewsFeedsUtilities.dll")]
        public void GetDocumentTitleTest()
        {
            const string xmlString = "<nitf><head><title>dette er en test</title></head></nitf>";
            using (StringReader sr = new StringReader(xmlString))
            {
                XPathDocument xPathDocument = new XPathDocument(sr); 
                const string expected = "dette er en test"; 
                string actual = NewsConverter_Accessor.GetDocumentTitle(xPathDocument);
                Assert.AreEqual(expected, actual);
            }
        }

        /// <summary>
        ///A test for CheckMessageForPictureReference
        ///</summary>
        [TestMethod]
        [DeploymentItem("NTBNewsFeedsUtilities.dll")]
        public void CheckMessageForPictureReferenceTest()
        {
            const string xmlString = "<nitf><body><body.content><media>dette er en test</media></body.content></body></nitf>";
            using (StringReader sr = new StringReader(xmlString))
            {
                XPathDocument xPathDocument = new XPathDocument(sr); 
                const bool expected = true; 
                bool actual = NewsConverter_Accessor.CheckMessageForPictureReference(xPathDocument, true);
                Assert.AreEqual(expected, actual);
            }
        }

        /// <summary>
        ///A test for CheckMessageForPictures
        ///</summary>
        [TestMethod]
        [DeploymentItem("NTBNewsFeedsUtilities.dll")]
        public void CheckMessageForPicturesTest()
        {
            const string xmlString = "<nitf><head><meta name='NTBBilderAntall' content='0' /></head></nitf>";
            using (StringReader sr = new StringReader(xmlString))
            {
                XPathDocument xPathDocument = new XPathDocument(sr); 
                const bool expected = false; 
                bool actual = NewsConverter_Accessor.CheckMessageForPictures(xPathDocument);
                Assert.AreEqual(expected, actual);
            }
            
            
        }

        /// <summary>
        ///A test for CreateMailMessage
        ///</summary>
        [TestMethod]
        [DeploymentItem("NTBNewsFeedsUtilities.dll")]
        public void CreateMailMessageTest_HasNoPictures()
        {
            const bool hasPictures = false; 
            const bool hasPictureReferences = false; 
            const string documentTitle = "Dette er en test"; 
            string expected = string.Empty; 
            string actual = NewsConverter_Accessor.CreateMailMessage(hasPictures, hasPictureReferences, documentTitle);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for CreateMailMessage
        ///</summary>
        [TestMethod]
        [DeploymentItem("NTBNewsFeedsUtilities.dll")]
        public void CreateMailMessageTest_HasNoPictureReference()
        {
            const bool hasPictures = true; 
            const bool hasPictureReferences = false; 
            const string documentTitle = "Dette er en test"; 
            string expected = string.Empty; 
            string actual = NewsConverter_Accessor.CreateMailMessage(hasPictures, hasPictureReferences, documentTitle);
            Assert.AreNotEqual(expected, actual);
        }


        /// <summary>
        ///A test for CreateMailMessage
        ///</summary>
        [TestMethod]
        [DeploymentItem("NTBNewsFeedsUtilities.dll")]
        public void CreateMailMessageTest()
        {
            const bool hasPictures = true; 
            const bool hasPictureReferences = true; 
            const string documentTitle = "Dette er en test"; 
            string expected = string.Empty; 
            string actual = NewsConverter_Accessor.CreateMailMessage(hasPictures, hasPictureReferences, documentTitle);
            Assert.AreEqual(expected, actual);
        }
    }
}
