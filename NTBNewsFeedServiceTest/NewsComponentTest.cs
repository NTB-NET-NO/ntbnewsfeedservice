﻿using NTB.NewsFeed.Components;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;


namespace NTBNewsFeedServiceTest
{
    
    
    /// <summary>
    ///This is a test class for NewsComponentTest and is intended
    ///to contain all NewsComponentTest Unit Tests
    ///</summary>
    [TestClass]
    public class NewsComponentTest
    {
        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext { get; set; }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for FilesToDelete
        ///</summary>
        [TestMethod]
        [DeploymentItem("NTBNewsFeedComponents.dll")]
        public void FilesToDeleteTest()
        {
            NewsComponent_Accessor target = new NewsComponent_Accessor(); 
            const int totalfiles = 100;
            const int maxFiles = 30;
            const int expected = 70; 
            int actual = target.FilesToDelete(totalfiles, maxFiles);
            Assert.AreEqual(expected, actual);
            
        }

        /// <summary>
        ///A test for DirectoryGetFiles
        ///</summary>
        [TestMethod]
        [DeploymentItem("NTBNewsFeedComponents.dll")]
        public void DirectoryGetFilesTest()
        {
            NewsComponent_Accessor target = new NewsComponent_Accessor(); 
            
            DirectoryInfo directoryInfoInputFolder = new DirectoryInfo(@"C:\Utvikling\NewsFeedService\Cache\MSN_NewsFeed"); 
            const int numberOfFilesToDelete = 30; 
            target.DirectoryGetFiles(directoryInfoInputFolder, numberOfFilesToDelete);
            FileInfo[] expected = directoryInfoInputFolder.GetFiles(); 
            

            Assert.IsNotNull(expected);
            Assert.IsInstanceOfType(expected, typeof(FileInfo[]));
            Assert.IsTrue(expected.Length <= 30);

            //actual = target.DirectoryGetFiles(directoryInfoInputFolder, numberOfFilesToDelete);
            //Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }
    }
}
