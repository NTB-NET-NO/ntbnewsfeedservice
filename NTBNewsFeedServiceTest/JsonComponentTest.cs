﻿using System.Configuration;
using NTB.NewsFeed.Components;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Xml;

namespace NTBNewsFeedServiceTest
{
    
    
    /// <summary>
    ///This is a test class for JsonComponentTest and is intended
    ///to contain all JsonComponentTest Unit Tests
    ///</summary>
    [TestClass]
    public class JsonComponentTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion
        
        /// <summary>
        ///A test for GetMaxFilesValue
        ///</summary>
        [TestMethod]
        [DeploymentItem("NTBNewsFeedComponents.dll")]
        public void GetMaxFilesValueTest()
        {
            string configSet = @"c:\utvikling\NewsFeedService\Config\NewsFeedConfiguration.xml";

            XmlDocument config = new XmlDocument();
            config.Load(configSet);

            XmlNode node = config.SelectSingleNode("//JsonComponent[@Enabled='True']");
            JsonComponent_Accessor target = new JsonComponent_Accessor(); 

            target.GetMaxFilesValue(node);

            int expected = 7;
            int actual = target.GetMaxFilesValue(node);

            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for GetInstanceNameValue
        ///</summary>
        [TestMethod]
        [DeploymentItem("NTBNewsFeedComponents.dll")]
        public void GetInstanceNameValueTest()
        {
            string configSet = @"c:\utvikling\NewsFeedService\Config\NewsFeedConfiguration.xml";

            XmlDocument config = new XmlDocument();
            config.Load(configSet);

            XmlNode node = config.SelectSingleNode("//JsonComponent[@Enabled='True']");
            JsonComponent_Accessor target = new JsonComponent_Accessor(); 

            target.GetMaxFilesValue(node);

            string expected = "AftenpostenGodMorgenJSON";
            string actual = target.GetInstanceNameValue(node);

            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for GetComponentEnabledValue
        ///</summary>
        [TestMethod]
        [DeploymentItem("NTBNewsFeedComponents.dll")]
        public void GetComponentEnabledValueTest()
        {
            string configSet = @"c:\utvikling\NewsFeedService\Config\NewsFeedConfiguration.xml";

            XmlDocument config = new XmlDocument();
            config.Load(configSet);

            XmlNode node = config.SelectSingleNode("//JsonComponent[@Enabled='True']");
            JsonComponent_Accessor target = new JsonComponent_Accessor(); 

            target.GetMaxFilesValue(node);

            bool expected = true;
            bool actual = target.GetComponentEnabledValue(node);

            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for GetOutputNameValue
        ///</summary>
        [TestMethod]
        [DeploymentItem("NTBNewsFeedComponents.dll")]
        public void GetOutputNameValueTest()
        {
            const string configSet = @"c:\utvikling\NewsFeedService\Config\NewsFeedConfiguration.xml";

            XmlDocument config = new XmlDocument();
            config.Load(configSet);

            XmlNode node = config.SelectSingleNode("//JsonComponent[@Enabled='True']");
            JsonComponent_Accessor target = new JsonComponent_Accessor(); 

            string expected = "aftenposten.json";
            string actual = target.GetOutputNameValue(node);

            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for GetPollDelayValue
        ///</summary>
        [TestMethod]
        [DeploymentItem("NTBNewsFeedComponents.dll")]
        public void GetPollDelayValueTest()
        {
            const string configSet = @"c:\utvikling\NewsFeedService\Config\NewsFeedConfiguration.xml";

            XmlDocument config = new XmlDocument();
            config.Load(configSet);

            XmlNode node = config.SelectSingleNode("//JsonComponent[@Enabled='True']");
            JsonComponent_Accessor target = new JsonComponent_Accessor(); 

            int expected = 10000;
            int actual = target.GetPollDelayValue(node);

            Assert.AreEqual(expected, actual);

        }

        /// <summary>
        ///A test for GetPollStyleValue
        ///</summary>
        [TestMethod]
        [DeploymentItem("NTBNewsFeedComponents.dll")]
        public void GetPollStyleValueTest()
        {
            const string configSet = @"c:\utvikling\NewsFeedService\Config\NewsFeedConfiguration.xml";

            XmlDocument config = new XmlDocument();
            config.Load(configSet);

            XmlNode node = config.SelectSingleNode("//JsonComponent[@Enabled='True']");
            JsonComponent_Accessor target = new JsonComponent_Accessor(); 

            PollStyle? expected = PollStyle.Continous; 
            PollStyle? actual = target.GetPollStyleValue(node);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for GetJsonErrorFolder
        ///</summary>
        [TestMethod]
        [DeploymentItem("NTBNewsFeedComponents.dll")]
        public void GetJsonErrorFolderTest()
        {
            const string configSet = @"c:\utvikling\NewsFeedService\Config\NewsFeedConfiguration.xml";

            XmlDocument config = new XmlDocument();
            config.Load(configSet);

            XmlNode node = config.SelectSingleNode("//JsonComponent[@Enabled='True']");
            JsonComponent_Accessor target = new JsonComponent_Accessor(); 

            string expected = @"C:\Utvikling\NewsFeedService\Error\AftenpostenJsonFeed"; 
            string actual = target.GetJsonErrorFolder(node);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for GetJsonFeedFolder
        ///</summary>
        [TestMethod]
        [DeploymentItem("NTBNewsFeedComponents.dll")]
        public void GetJsonFeedFolderTest()
        {
            const string configSet = @"c:\utvikling\NewsFeedService\Config\NewsFeedConfiguration.xml";

            XmlDocument config = new XmlDocument();
            config.Load(configSet);

            XmlNode node = config.SelectSingleNode("//JsonComponent[@Enabled='True']");
            JsonComponent_Accessor target = new JsonComponent_Accessor(); 

            string expected = @"C:\Utvikling\NewsFeedService\wwwroot\json"; 
            string actual = target.GetJsonFeedFolder(node);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for GetJsonDoneFolderValue
        ///</summary>
        [TestMethod]
        [DeploymentItem("NTBNewsFeedComponents.dll")]
        public void GetJsonDoneFolderValueTest()
        {
            const string configSet = @"c:\utvikling\NewsFeedService\Config\NewsFeedConfiguration.xml";

            XmlDocument config = new XmlDocument();
            config.Load(configSet);

            XmlNode node = config.SelectSingleNode("//JsonComponent[@Enabled='True']");
            JsonComponent_Accessor target = new JsonComponent_Accessor(); 

            string expected = @"C:\Utvikling\NewsFeedService\Done\AftenpostenJsonFeed"; 
            string actual = target.GetJsonDoneFolderValue(node);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for GetJsonInputFolderValue
        ///</summary>
        [TestMethod]
        [DeploymentItem("NTBNewsFeedComponents.dll")]
        public void GetJsonInputFolderValueTest()
        {
            const string configSet = @"c:\utvikling\NewsFeedService\Config\NewsFeedConfiguration.xml";

            XmlDocument config = new XmlDocument();
            config.Load(configSet);

            XmlNode node = config.SelectSingleNode("//JsonComponent[@Enabled='True']");
            JsonComponent_Accessor target = new JsonComponent_Accessor(); 

            string expected = @"C:\Utvikling\NewsFeedService\Input\Aftenposten_GodMorgenXMLFeed"; 
            string actual = target.GetJsonInputFolderValue(node);
            Assert.AreEqual(expected, actual);
        }
    }
}
