﻿using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NTB.NewsFeed.Utilities;

namespace NTBNewsFeedServiceTest
{
    /// <summary>
    ///     This is a test class for FileUtilitiesTest and is intended
    ///     to contain all FileUtilitiesTest Unit Tests
    /// </summary>
    [TestClass]
    public class FileUtilitiesTest
    {
        /// <summary>
        ///     Gets or sets the test context which provides
        ///     information about and functionality for the current test run.
        /// </summary>
        public TestContext TestContext { get; set; }

        #region Additional test attributes

        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //

        #endregion

        /// <summary>
        ///     A test for CheckAccessableFile - shall return an exception if not it fails
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof (Exception), AllowDerivedTypes = true)]
        public void CheckAccessableFileTest_ShouldThrowException()
        {
            var target = new FileUtilities(); 
            var fi =
                new FileInfo(
                    @"C:\Utvikling\NewsFeedService\Done\MSN_NewsFeed\2013-08-11_22-55-54_Nyhetstjenesten_SPO_TAB_fotball-belgisk-resmsxg.xml");
            target.CheckAccessableFile(fi);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///     A test for CheckAccessableFile
        /// </summary>
        [TestMethod]
        public void CheckAccessableFileTest_ShouldNotThrowException()
        {
            var target = new FileUtilities(); 
            var fi =
                new FileInfo(
                    @"C:\Utvikling\NewsFeedService\Done\MSN_NewsFeed\2013-08-11_22-55-54_Nyhetstjenesten_SPO_TAB_fotball-belgisk-resmsg.xml");
            target.CheckAccessableFile(fi);
            // Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///     A test for IsFileAccessable
        /// </summary>
        [TestMethod]
        [DeploymentItem("NTBNewsFeedsUtilities.dll")]
        public void IsFileAccessableTest()
        {
            var target = new FileUtilities_Accessor(); 
            const string filename =
                @"C:\Utvikling\NewsFeedService\Done\MSN_NewsFeed\2013-08-11_22-55-54_Nyhetstjenesten_SPO_TAB_fotball-belgisk-resmsg.xml";
            const bool expected = true;
            bool actual = target.IsFileAccessable(filename);
            Assert.AreEqual(expected, actual);
            // Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///     A test for GetCacheFiles
        /// </summary>
        [TestMethod]
        public void GetCacheFilesTest()
        {
            var target = new FileUtilities(); 
            const string directoryName = @"C:\Utvikling\NewsFeedService\Cache\MSN_NewsFeed";

            List<FileInfo> list = target.GetCacheFiles(directoryName);

            Assert.IsNotNull(list);
            Assert.IsInstanceOfType(list, typeof (List<FileInfo>));
            Assert.IsTrue(list.Count != 0);
        }

        /// <summary>
        ///     A test for FindFileInCache
        /// </summary>
        [TestMethod]
        public void FindFileInCacheTest()
        {
            var target = new FileUtilities();
            const string directoryName = @"C:\Utvikling\NewsFeedService\Cache\MSN_NewsFeed";
            const string filename = "RED130811_121832_jb-1.xml";
            List<FileInfo> cacheFiles = target.GetCacheFiles(directoryName);
            const bool expected = true;
            bool actual = target.FindFileInCache(filename, cacheFiles);
            Assert.AreEqual(expected, actual);
            // Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///     A test for FindFileInCacheByDocumentId
        /// </summary>
        [TestMethod]
        public void FindFileInCacheByDocumentIdTest()
        {
            var target = new FileUtilities();
            const string directoryName = @"C:\Utvikling\NewsFeedService\Cache\MSN_NewsFeed";

            const string documentId = "RED130811_150401_uv";
            List<FileInfo> cacheFiles = target.GetCacheFiles(directoryName);
            const bool expected = true;
            bool actual = target.FindFileInCacheByDocumentId(documentId, cacheFiles);
            Assert.AreEqual(expected, actual);
            // Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///     A test for FindFileVersionByVersionNumber
        /// </summary>
        [TestMethod]
        public void FindNotFileVersionByVersionNumberTest()
        {
            var target = new FileUtilities();
            const string directoryName = @"C:\Utvikling\NewsFeedService\Cache\MSN_NewsFeed";

            const string documentId = "RED130811_150401_uv";
            List<FileInfo> cacheFiles = target.GetCacheFiles(directoryName);
            const int versionNumber = 1;

            const bool expected = false;
            bool actual = 
            target.FindFileVersionByVersionNumber(versionNumber, documentId, cacheFiles);
            Assert.AreEqual(expected, actual);
            // Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///     A test for CreatePreviousCacheFileName
        /// </summary>
        [TestMethod]
        public void CreatePreviousCacheFileNameTest()
        {
            var target = new FileUtilities(); 
            const string documentId = "RED130811_150401_uv";
            const int documentVersion = 2; // 
            const string expected = "RED130811_150401_uv-1.xml"; 
            string actual = target.CreatePreviousCacheFileName(documentId, documentVersion);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///     A test for CreatePreviousCacheFileName
        /// </summary>
        [TestMethod]
        public void CreatePreviousCacheFileNameReturnEmptyTest()
        {
            var target = new FileUtilities(); 
            const string documentId = "RED130811_150401_uv";
            const int documentVersion = 1; // 
            string expected = string.Empty; 
            string actual = target.CreatePreviousCacheFileName(documentId, documentVersion);
            Assert.AreEqual(expected, actual);
        }
    }
}