﻿using NTB.NewsFeed.Utilities;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace NTBNewsFeedServiceTest
{
    /// <summary>
    ///This is a test class for XmlUtilitiesTest and is intended
    ///to contain all XmlUtilitiesTest Unit Tests
    ///</summary>
    [TestClass]
    public class XmlUtilitiesTest
    {
        private TestContext _testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return _testContextInstance;
            }
            set
            {
                _testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for GetDocumentId
        ///</summary>
        [TestMethod]
        public void GetDocumentIdTest()
        {
            XmlUtilities target = new XmlUtilities(); 
            string filename = @"C:\Utvikling\NewsFeedService\Done\MSN_NewsFeed\2013-08-11_22-55-54_Nyhetstjenesten_SPO_TAB_fotball-belgisk-resmsg.xml"; 
            string expected = @"RED130810_220906_eb"; 
            string actual;
            actual = target.GetDocumentId(filename);
            Assert.AreEqual(expected, actual);
            // Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for GetDocumentVersion
        ///</summary>
        [TestMethod]
        public void GetDocumentVersionTest()
        {
            XmlUtilities target = new XmlUtilities(); 
            string filename =
                @"C:\Utvikling\NewsFeedService\Done\MSN_NewsFeed\2013-08-11_22-55-54_Nyhetstjenesten_SPO_TAB_fotball-belgisk-resmsg.xml";
            int expected = 3; 
            int actual;
            actual = target.GetDocumentVersion(filename);
            Assert.AreEqual(expected, actual);
            // Assert.Inconclusive("Verify the correctness of this test method.");
        }
    }
}
