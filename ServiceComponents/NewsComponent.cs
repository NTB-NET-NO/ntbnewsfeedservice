﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NewsComponent.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   Defines the NewsComponent type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace NTB.NewsFeed.Components
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Configuration;
    using System.IO;
    using System.Linq;
    using System.Net.Mail;
    using System.Threading;
    using System.Timers;
    using System.Xml;
    
    using log4net;

    using NTB.NewsFeed.Utilities;

    // Adding support for log4net

    /// <summary>5
    /// The news component.
    /// </summary>
    public partial class NewsComponent : Component, IBaseServiceComponent
    {
        /// <summary>
        /// Static logger
        /// </summary>
        protected static readonly ILog Logger = LogManager.GetLogger(typeof(MainServiceComponent));

        /// <summary>
        /// Busy status event
        /// </summary>
        private readonly AutoResetEvent busyEvent = new AutoResetEvent(true);

        /// <summary>
        /// Exit control event
        /// </summary>
        private readonly AutoResetEvent stopEvent = new AutoResetEvent(false);

        /// <summary>
        /// Buffer size for the file watcher
        /// </summary>
        /// <remarks>
        ///   <para>Defines the internal buffer size the FileSystemWatcher when using this polling.
        /// Should not be set to high. The instance will fall back to contious polling on buffer overflows.</para>
        ///   <para>Default value: <c>4096</c></para>
        /// </remarks>
        protected int BufferSize = 4096;

        /// <summary>
        /// Flag to say if this component is working or not. I have a feeling we have another parameter for this though.
        /// </summary>
        protected bool BusyFlag = false;

        /// <summary>
        /// Flag to indicate sucessful configure. Instance will not start polling nor handle messages if not properly configured
        /// </summary>
        /// <remarks>Holds the configured status of the instance. <c>True</c> means successfully configured</remarks>
        protected bool Configured;

        /// <summary>
        /// Dictionary to map ID to encodings, which we can use to say which type of encoding the output file shall have
        /// </summary>
        protected Dictionary<string, string> DocumentEncodings = new Dictionary<string, string>();

        /// <summary>
        /// Dictionary to map ID to file extensions, which we can use to say which type of encoding the output file shall have
        /// </summary>
        protected Dictionary<string, string> DocumentExtensions = new Dictionary<string, string>();

        /// <summary>
        /// Body for email notifications
        /// </summary>
        /// <remarks>The body is built during processing and used when sending the email when processing completes</remarks>
        protected string EmailBody;

        /// <summary>
        /// Send Email-notifications when a new messages is processed
        /// </summary>
        /// <remarks>Set to an email address. Multiple adresses are supported, separate with <c>;</c></remarks>
        protected string EmailNotification;

        /// <summary>
        /// Subject for email notifications
        /// </summary>
        /// <remarks>The subject is built during processing and used when sending the email when processing completes</remarks>
        protected string EmailSubject;

        /// <summary>
        /// Error-Retry flag
        /// </summary>
        /// <remarks>The flag is being set if a network or EWS error is encountered. Current operations are being aborted for later retry.</remarks>
        protected bool ErrorRetry;

        /// <summary>
        /// File folder where completed files are stored
        /// </summary>
        /// <remarks>
        ///   <para>File folder where completed files are archived. Subdirectories for years, months and dates are created.</para>
        ///   <para>If this is not set, imported files are deleted instead of archived.</para>
        /// </remarks>
        protected string FileDoneFolder;

        /// <summary>
        /// Error file folder
        /// </summary>
        /// <remarks>
        ///   <para>File folder where failing files are stored.</para>
        ///   <para>If this is not set, failing files are deleted instead of saved.</para>
        /// </remarks>
        protected string FileErrorFolder;

        /// <summary>
        /// Input file filter
        /// </summary>
        /// <remarks>
        ///   <para>Defines what file types are being read from <see cref="FileInputFolder"/>.</para>
        ///   <para>Default value: <c>NTBIDType.None</c></para>
        /// </remarks>
        protected string FileFilter = "*.xml";

        /// <summary>
        /// Input file folder
        /// </summary>
        /// <remarks>
        /// File folder where incoming files are read from. Inputs can be modified by <see cref="FileFilter"/> and <see cref="IncludeSubdirs"/>
        /// </remarks>
        protected string FileInputFolder;

        /// <summary>
        /// Output file folder
        /// </summary>
        /// <remarks>
        /// File folder where incoming files are read from. Inputs can be modified by <see cref="FileFilter"/> and <see cref="IncludeSubdirs"/>
        /// </remarks>
        protected Dictionary<string, string> FileOutputFolders = new Dictionary<string, string>();

        /// <summary>
        /// The file xslt folder.
        /// </summary>
        protected string FileXsltFolder;

        /// <summary>
        /// Watch subdirs for new files as well
        /// </summary>
        /// <remarks>
        ///   <para>Defines if the instance should look in subdirectories within <see cref="FileInputFolder"/>.</para>
        ///   <para>Default value: <c>False</c></para>
        /// </remarks>
        protected bool IncludeSubdirs = false;

        /// <summary>
        /// The configured interval for this instance, used for Continous polling
        /// </summary>
        /// <remarks>
        ///   <para>Indicates the interval time in seconds for <c>Continous</c>polling. 60 means that the job runs every minute.</para>
        ///   <para>Default value: <c>60</c></para>
        /// </remarks>
        protected int Interval = 60;

        /// <summary>
        /// The configured job name for this instance
        /// </summary>
        /// <remarks>Internal field, accessed through interface implemenation <see cref="InstanceName"/></remarks>
        protected string Name;

        /// <summary>
        /// PollDelay is used to make sure that the file is ready to be accessed. 
        /// </summary>
        protected int PollDelay;

        /// <summary>
        /// The configured schedule for this instance
        /// </summary>
        /// <remarks>This is set to a time of day and is used by the <c>Scheduled</c> pollstyle. Work is triggered at the given time of day.</remarks>
        protected string Schedule;

        /// <summary>
        /// The file xslt folder.
        /// </summary>
        protected string XsltFile;

        /// <summary>
        /// Initializes a new instance of the <see cref="NewsComponent"/> class.
        /// </summary>
        public NewsComponent()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NewsComponent"/> class.
        /// </summary>
        /// <param name="container">
        /// The container.
        /// </param>
        public NewsComponent(IContainer container)
        {
            container.Add(this);

            this.InitializeComponent();
        }

        /// <summary>
        /// Gets or sets the dir cache folder.
        /// </summary>
        public string DirCacheFolder { get; set; }

        /// <summary>
        /// Gets or sets the dir done folder.
        /// </summary>
        public string DirDoneFolder { get; set; }

        /// <summary>
        /// Gets or sets the dir error folder.
        /// </summary>
        public string DirErrorFolder { get; set; }

        /// <summary>
        /// Gets or sets the max files.
        /// </summary>
        public int MaxFiles { get; set; }

        /// <summary>
        /// Gets or sets the xslt feed file.
        /// </summary>
        public string XsltFeedFile { get; set; }

        /// <summary>
        /// Gets or sets the xml feed folder.
        /// </summary>
        public string XmlFeedFolder { get; set; }

        /// <summary>
        /// Gets or sets the output name.
        /// </summary>
        public string OutputName { get; set; }

        /// <summary>
        /// Gets or sets the error receivers.
        /// </summary>
        private List<string> ErrorReceivers { get; set; }

        /// <summary>
        /// Gets or sets the x path expression.
        /// </summary>
        protected string XPathExpression { get; set; }

        /// <summary>
        /// Gets a value indicating whether enabled.
        /// </summary>
        public bool Enabled
        {
            get
            {
                return this.enabled;
            }
        }

        /// <summary>
        /// Gets the instance name.
        /// </summary>
        public string InstanceName
        {
            get
            {
                return this.Name;
            }
        }

        /// <summary>
        /// Gets the operation mode.
        /// </summary>
        public OperationMode OperationMode
        {
            get
            {
                return OperationMode.Converter;
            }
        }

        /// <summary>
        /// Gets the poll style.
        /// </summary>
        public PollStyle PollStyle
        {
            get
            {
                return this.pollStyle;
            }
        }

        /// <summary>
        /// Gets the component state.
        /// </summary>
        public ComponentState ComponentState
        {
            get
            {
                return this.componentState;
            }
        }

        /// <summary>
        /// The configure.
        /// </summary>
        /// <param name="configNode">
        /// The config node.
        /// </param>
        public void Configure(XmlNode configNode)
        {
            Logger.Debug("Node: " + configNode.Name);
            if (configNode.Attributes != null)
            {
                Logger.Debug("Name attribut: " + configNode.Attributes.GetNamedItem("Name"));

                // Basic configuration sanity check
                if (configNode.Name != "NewsComponent" || configNode.Attributes.GetNamedItem("Name") == null)
                {
                    throw new ArgumentException("The XML configuration node passed is invalid", "configNode");
                }

                // Getting the name of this Component instance
                try
                {
                    this.Name = configNode.Attributes["Name"].Value;
                    this.enabled = Convert.ToBoolean(configNode.Attributes["Enabled"].Value);
                    ThreadContext.Stacks["NDC"].Push(this.InstanceName);
                    this.MaxFiles = Convert.ToInt32(configNode.Attributes["MaxFiles"].Value);

                    if (configNode.Attributes["ErrorNotification"] != null)
                    {
                        this.ErrorReceivers = configNode.Attributes["ErrorNotification"].Value.Split(',').ToList();
                    }

                    if (this.MaxFiles == 0)
                    {
                        this.MaxFiles = Convert.ToInt32(ConfigurationManager.AppSettings["DefaultMaxFiles"]);
                    }

                    if (configNode.Attributes["OutputName"] != null)
                    {
                        this.OutputName = configNode.Attributes["OutputName"].Value;
                    }

                    if (string.IsNullOrEmpty(this.OutputName))
                    {
                        this.OutputName = ConfigurationManager.AppSettings["DefaultFeedName"];
                    }

                    if (configNode.Attributes["Pause"] != null)
                    {
                        this.PollDelay = Convert.ToInt32(configNode.Attributes["Pause"].Value);

                        if (this.PollDelay < 1000)
                        {
                            this.PollDelay = this.PollDelay * 1000;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.Fatal("Not possible to configure this job instance!", ex);
                }

                // Basic operation
                try
                {
                    this.pollStyle =
                        (PollStyle)Enum.Parse(typeof(PollStyle), configNode.Attributes["PollStyle"].Value, true);
                }
                catch (Exception ex)
                {
                    ThreadContext.Stacks["NDC"].Pop();
                    throw new ArgumentException("Invalid or missing OperationMode values in XML configuration node", ex);
                }
            }

            try
            {
                // Find the file folder to work with
                XmlNode folderNode = configNode.SelectSingleNode("XMLFeedFolder");

                if (folderNode != null)
                {
                    this.XmlFeedFolder = folderNode.InnerText;

                    if (!Directory.Exists(this.XmlFeedFolder))
                    {
                        Directory.CreateDirectory(this.XmlFeedFolder);
                    }
                }

                Logger.InfoFormat(
                    "Converter job - Polling: {0} / XMLFeed Folder: {1} / Enabled: {2}", 
                    Enum.GetName(typeof(PollStyle), this.pollStyle), 
                    this.XmlFeedFolder, 
                    this.enabled);
            }
            catch (Exception ex)
            {
                Logger.Fatal("Not possible to get XMLFeed Folder", ex);
            }

            try
            {
                // Find the picture file folder to work with
                XmlNode folderNode = configNode.SelectSingleNode("XMLPictureFolder");

                if (folderNode != null)
                {
                    this.XmlPictureFolder = folderNode.InnerText;

                    if (!Directory.Exists(this.XmlFeedFolder))
                    {
                        Directory.CreateDirectory(this.XmlFeedFolder);
                    }
                }

                Logger.InfoFormat(
                    "Converter job - Polling: {0} / XML Picture Folder: {1} / Enabled: {2}",
                    Enum.GetName(typeof(PollStyle), this.pollStyle),
                    this.XmlFeedFolder,
                    this.enabled);
            }
            catch (Exception ex)
            {
                Logger.Fatal("Not possible to get Picture-folder", ex);
            }

            try
            {
                // Find the file folder to work with
                XmlNode folderNode = configNode.SelectSingleNode("XSLTFeedFile");

                if (folderNode != null)
                {
                    this.XsltFeedFile = folderNode.InnerText;

                    string xsltFeedFilePath = Path.GetDirectoryName(this.XsltFeedFile);
                    if (!string.IsNullOrEmpty(xsltFeedFilePath))
                    {
                        if (!Directory.Exists(xsltFeedFilePath))
                        {
                            Directory.CreateDirectory(xsltFeedFilePath);
                        }
                    }
                }

                Logger.InfoFormat(
                    "Converter job - Polling: {0} / XSLTFeed File : {1} / Enabled: {2}", 
                    Enum.GetName(typeof(PollStyle), this.pollStyle), 
                    this.XsltFeedFile, 
                    this.enabled);
            }
            catch (Exception ex)
            {
                Logger.Fatal("Not possible to get Error-folder", ex);
            }

            try
            {
                // Find the file folder to work with
                XmlNode folderNode = configNode.SelectSingleNode("XMLErrorFolder");

                // configNode.SelectSingleNode("Folder").Attributes("XMLOutputFolder");
                if (folderNode != null)
                {
                    this.DirErrorFolder = folderNode.InnerText;

                    if (!Directory.Exists(this.DirErrorFolder))
                    {
                        Directory.CreateDirectory(this.DirErrorFolder);
                    }
                }

                Logger.InfoFormat(
                    "Converter job - Polling: {0} / Directory Cache folder: {1} / Enabled: {2}", 
                    Enum.GetName(typeof(PollStyle), this.pollStyle), 
                    this.DirErrorFolder, 
                    this.enabled);
            }
            catch (Exception ex)
            {
                Logger.Fatal("Not possible to get Error-folder", ex);
            }

            try
            {
                // Find the file folder to work with
                XmlNode folderNode = configNode.SelectSingleNode("XMLDoneFolder");

                // configNode.SelectSingleNode("Folder").Attributes("XMLOutputFolder");
                if (folderNode != null)
                {
                    this.DirDoneFolder = folderNode.InnerText;

                    if (!Directory.Exists(this.DirDoneFolder))
                    {
                        Directory.CreateDirectory(this.DirDoneFolder);
                    }
                }

                Logger.InfoFormat(
                    "Converter job - Polling: {0} / Directory Cache folder: {1} / Enabled: {2}", 
                    Enum.GetName(typeof(PollStyle), this.pollStyle), 
                    this.DirDoneFolder, 
                    this.enabled);
            }
            catch (Exception ex)
            {
                Logger.Fatal("Not possible to get Directory Cache-folder", ex);
            }

            try
            {
                // Find the file folder to work with
                XmlNode folderNode = configNode.SelectSingleNode("XMLCacheOutputFolder");

                // configNode.SelectSingleNode("Folder").Attributes("XMLOutputFolder");
                if (folderNode != null)
                {
                    this.DirCacheFolder = folderNode.InnerText;

                    if (!Directory.Exists(this.DirCacheFolder))
                    {
                        Logger.Info("Creating " + this.DirCacheFolder);
                        Directory.CreateDirectory(this.DirCacheFolder);
                    }
                }

                Logger.InfoFormat(
                    "Converter job - Polling: {0} / Directory Cache folder: {1} / Enabled: {2}", 
                    Enum.GetName(typeof(PollStyle), this.pollStyle), 
                    this.DirCacheFolder, 
                    this.enabled);
            }
            catch (Exception ex)
            {
                Logger.Fatal("Not possible to get Directory Cache-folder", ex);
            }

            try
            {
                // Find the file folder to work with
                XmlNode folderNode = configNode.SelectSingleNode("XMLInputFolder");

                if (folderNode != null)
                {
                    this.FileInputFolder = folderNode.InnerText;

                    if (!Directory.Exists(this.FileInputFolder))
                    {
                        Logger.Info("Creating " + this.FileInputFolder);
                        Directory.CreateDirectory(this.FileInputFolder);
                    }
                }

                Logger.InfoFormat(
                    "Converter job - Polling: {0} / File input folder: {1} / Enabled: {2}", 
                    Enum.GetName(typeof(PollStyle), this.pollStyle), 
                    this.FileInputFolder, 
                    this.enabled);
            }
            catch (Exception ex)
            {
                Logger.Fatal("Not possible to get XML-Input-folder", ex);
            }

            try
            {
                XmlNode folderNode = configNode.SelectSingleNode("XSLTFile");

                if (folderNode != null)
                {
                    string fileXsltFilePath = folderNode.InnerText;
                    FileInfo xsltFolder = new FileInfo(fileXsltFilePath);

                    // I believe this should work todo: test!
                    this.FileXsltFolder = xsltFolder.Directory.ToString();

                    // Checking if the directory / path existed or not
                    if (!Directory.Exists(this.FileXsltFolder))
                    {
                        Logger.Info("Creating " + this.FileXsltFolder);
                        Directory.CreateDirectory(this.FileXsltFolder);
                    }

                    this.XsltFile = folderNode.InnerText;
                }

                Logger.InfoFormat(
                    "Converter job - Polling: {0} / File XSLT folder: {1} / Enabled: {2}", 
                    Enum.GetName(typeof(PollStyle), this.pollStyle), 
                    this.FileXsltFolder, 
                    this.enabled);
            }
            catch (Exception ex)
            {
                Logger.Fatal("Not possible to get XSLT-folder", ex);
            }

            try
            {
                // Find the file folder to work with
                XmlNodeList folderNodes = configNode.SelectNodes("XMLOutputFolders/XMLOutputFolder");

                foreach (XmlNode folderNode in folderNodes)
                {
                    // configNode.SelectSingleNode("Folder").Attributes("XMLOutputFolder");
                    if (folderNode != null)
                    {
                        this.FileOutputFolders.Add(folderNode.Attributes["Id"].Value, folderNode.InnerText);
                        this.DocumentEncodings.Add(
                            folderNode.Attributes["Id"].Value, 
                            folderNode.Attributes["Encoding"] != null
                                ? folderNode.Attributes["Encoding"].Value
                                : "iso-8859-1");

                        this.DocumentExtensions.Add(
                            folderNode.Attributes["Id"].Value, 
                            folderNode.Attributes["Extention"] != null
                                ? folderNode.Attributes["Extention"].Value
                                : "xml");
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Fatal("Not possible to get output-folder", ex);
            }

            // XpathExpression
            try
            {
                XmlNode folderNode = configNode.SelectSingleNode("XpathExpression");

                if (folderNode != null)
                {
                    this.XPathExpression = folderNode.InnerText;
                }

                Logger.Info("XPathFilter for this job: " + this.XPathExpression);
            }
            catch (Exception ex)
            {
                Logger.Fatal("Not possible to get XPathFilter", ex);
            }

            // This is where the FileFolder creation shall end
            if (this.enabled)
            {
                

                // Checking if file folders exists
                string fileOutputFolder = string.Empty;

                try
                {
                    if (!Directory.Exists(this.FileInputFolder))
                    {
                        Directory.CreateDirectory(this.FileInputFolder);
                    }

                    foreach (KeyValuePair<string, string> kvp in this.FileOutputFolders)
                    {
                        fileOutputFolder = kvp.Value;
                        if (!Directory.Exists(fileOutputFolder))
                        {
                            Directory.CreateDirectory(fileOutputFolder);
                        }
                    }
                }
                catch (Exception ex)
                {
                    ThreadContext.Stacks["NDC"].Pop();
                    throw new ArgumentException("Invalid, unknown or missing file folder: " + fileOutputFolder, ex);
                }

                

                #region Set up polling

                try
                {
                    // Switch on pollstyle
                    switch (this.pollStyle)
                    {
                        case PollStyle.Continous:
                            this.Interval = Convert.ToInt32(configNode.Attributes["Interval"].Value);
                            if (this.Interval == 0)
                            {
                                this.Interval = 5000;
                            }

                            this.pollTimer.Interval = this.Interval; // short startup - interval * 1000;
                            Logger.DebugFormat("Poll interval: {0} seconds", this.Interval);

                            break;

                        case PollStyle.Scheduled:

                            /* 
                            schedule = configNode.Attributes["Schedule"].Value;
                            TimeSpan s = Utilities.GetScheduleInterval(schedule);
                            logger.DebugFormat("Schedule: {0} Calculated time to next run: {1}", schedule, s);
                            pollTimer.Interval = s.TotalMilliseconds;
                            */
                            throw new NotSupportedException("Invalid polling style for this job type");

                        case PollStyle.FileSystemWatch:

                            // Check for config overrides
                            if (configNode.Attributes.GetNamedItem("BufferSize") != null)
                            {
                                this.BufferSize = Convert.ToInt32(configNode.Attributes["BufferSize"].Value);
                            }

                            Logger.DebugFormat("FileSystemWatcher buffer size: {0}", this.BufferSize);

                            // Set values
                            this.fileWatcher.Path = this.FileInputFolder;
                            this.fileWatcher.Filter = this.FileFilter;
                            this.fileWatcher.IncludeSubdirectories = this.IncludeSubdirs;
                            this.fileWatcher.InternalBufferSize = this.BufferSize;

                            // Do not start the event watcher here, wait until after the startup cleaning job
                            this.pollTimer.Interval = 5000; // short startup;

                            break;

                        default:

                            // Unsupported pollstyle for this object
                            throw new NotSupportedException("Invalid polling style for this job type");
                    }
                }
                catch (Exception ex)
                {
                    ThreadContext.Stacks["NDC"].Pop();
                    throw new ArgumentException(
                        "Invalid or missing PollStyle-specific values in XML configuration node: " + ex.Message, 
                        ex);
                }

                #endregion
            }

            // Finish configuration
            ThreadContext.Stacks["NDC"].Pop();
            this.Configured = true;
        }

        /// <summary>
        /// The start.
        /// </summary>
        /// <exception cref="NewsFeedsException">
        /// A News Feed Exception occurs if there is an error here
        /// </exception>
        /// <exception cref="Exception">
        /// An Exception occurs if there is an error here
        /// </exception>
        public void Start()
        {
            if (!this.Configured)
            {
                throw new NewsFeedsException(
                    "NewsComponent is not properly configured. NewsComponent::Start() Aborted!");
            }

            if (!this.enabled)
            {
                throw new NewsFeedsException("NewsComponent is not enabled. NewsComponent::Start() Aborted!");
            }

            this.Starting = true;
            Logger.Info("In Start() " + this.InstanceName + ".");

            this.ConvertAndCombine();

            this.stopEvent.Reset();

            this.busyEvent.Set();

            this.componentState = ComponentState.Running;

            this.pollTimer.Start();

            this.Starting = false;
        }

        ///// <summary>
        ///// DEPRECATED: Running the converter
        ///// </summary>
        ///// <param name="files"></param>
        // private void ConvertFiles(List<string> files)
        // {

        // // Now we shall check if we have the document and the same version in the Cache
        // Logger.Debug("We have received " + files.Count + " files");

        // // Looping over the files
        // foreach (string file in files)
        // {
        // NewsConverter newsConverter = new NewsConverter
        // {
        // XmlFile = file,
        // XsltFile = XsltFile,
        // OutPath = DirCacheFolder,
        // DonePath = DirDoneFolder,
        // ErrorPath = DirErrorFolder,
        // XPathFilter = XPathExpression,
        // // DocumentId = documentId,
        // // DocumentVersion = documentVersion
        // };
        // newsConverter.ConvertFile();

        // newsConverter.Dispose();

        // }
        // }

        /// <summary>
        /// The stop.
        /// </summary>
        /// <exception cref="NewsFeedsException">
        /// A News Feed Exception occurs if there is an error here
        /// </exception>
        public void Stop()
        {
            if (!this.Configured)
            {
                throw new NewsFeedsException("NewsComponent is not properly configured. NFFComponent::Stop() Aborted");
            }

            if (!this.enabled)
            {
                throw new NewsFeedsException("NewsComponent is not properly configured. NFFComponent::Stop() Aborted");
            }

            const double Epsilon = 0;
            if (!this.pollTimer.Enabled
                && (this.ErrorRetry || this.pollStyle != PollStyle.FileSystemWatch
                    || Math.Abs(this.pollTimer.Interval - 5000) < Epsilon))
            {
                Logger.InfoFormat("Waiting for instance to complete work. Job {0}", this.InstanceName);
            }

            // Signal Stop
            this.stopEvent.Set();

            // Stop events
            this.fileWatcher.EnableRaisingEvents = false;

            if (!this.busyEvent.WaitOne(30000))
            {
                Logger.InfoFormat(
                    "Instance did not complete properly. Data may be inconsistent. Job: {0}", 
                    this.InstanceName);
            }

            this.componentState = ComponentState.Halted;

            // Kill polling
            this.pollTimer.Stop();
        }

        /// <summary>
        /// Check if File Has Picture is used to check if the message has a picture. If it has, we shall check if the picture is in place
        /// </summary>
        /// <param name="filename">
        /// Contains the whole path to the file
        /// </param>
        /// <returns>
        /// Returns an XmlNodeList if we find a picture, null otherwise
        /// </returns>
        private XmlNodeList CheckIfFileHasPicture(string filename)
        {
            // Checking if we have a filename or not
            if (string.IsNullOrEmpty(filename))
            {
                return null;
            }

            // Get the document and and put it into a XpathDocument object so we can get some information out of it
            var xmlDoc = new XmlDocument();
            xmlDoc.Load(filename);

            // Create the document as a node
            var documentNode = xmlDoc.DocumentElement;

            // Use the xpathString and get an XmlNodeList out 
            if (documentNode == null)
            {
                return null;
            }
            
            // Check if the file has pictures
            const string XpathString = "//media[@media-type='image']";

            var nodeList = documentNode.SelectNodes(XpathString);

            // We check if the file has pictures by finding out that we have an object or not
            if (nodeList != null && nodeList.Count > 0)
            {
                return nodeList;
            }

            return null;
        }

        private bool CheckIfPicturesAreAvailable(List<string> filenames)
        {
            // Should we do a task here, just to files
            var fileExists = false;
            foreach (var filename in filenames)
            {
                /*
                 * We need to know the picture area
                 * Se we need to recode this Component a bit so we can define the picture folder
                 */
                var fileInfo = new FileInfo(this.XmlPictureFolder + "/" + filename);

                // We are checking if the filename exists. If it does, we shall set the variable to true
                // If there are more than one picture, we will check for each. If one is missing, we will return false 
                // and so we can check if the package is complete or not
                if (fileInfo.Exists)
                {
                    fileExists = true;
                }
            }

            return fileExists;
        }

        /// <summary>
        /// The get picture name.
        /// </summary>
        /// <param name="nodeList">
        /// List of nodes we are to loop over.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        private List<string> GetPictureNames(XmlNodeList nodeList)
        {
            var stringList = new List<string>();

            // Now we shall loop over the nodelist and check if the files exists
            foreach (XmlNode node in nodeList)
            {
                // Define the picture string 
                string pictureString;

                // the picture on the disk is either thumb_, high_, low_ and then the filename we are now about to check
                var pictureXpath = "media-reference/@source";
                var pictureNode = node.SelectSingleNode(pictureXpath);
                if (pictureNode != null)
                {                   
                    pictureString = pictureNode.Value;

                    // Then get the mime-type for this file
                    pictureXpath = "media-reference/@mime-type";
                    pictureNode = node.SelectSingleNode(pictureXpath);
                    if (pictureNode != null) 
                    { 
                        if (pictureNode.Value == "image/jpeg")
                        {
                            pictureString += ".jpg";
                        }
                    }

                    stringList.Add(pictureString);
                }
            }
            return stringList;
        }

        /// <summary>
        /// The convert and combine.
        /// </summary>
        /// <exception cref="Exception">
        /// Throws an exception on error
        /// </exception>
        private void ConvertAndCombine()
        {
            // I first want to check input folder and see if there are files in there. If there are, they are to be converted
            var directoryInfoInputFolder = new DirectoryInfo(this.FileInputFolder);
            var directoryInfoCacheFolder = new DirectoryInfo(this.DirCacheFolder);

            var fileInfos = directoryInfoInputFolder.GetFiles();

            // Getting the total files to convert
            var totalfiles = fileInfos.Length;

            // Send information to logfile
            Logger.Info("About to convert " + totalfiles + " from the input folder");

            if (totalfiles == 0)
            {
                return;
            }

            // Get the files we are to convert
            // We are getting this list before we are waiting. This so we are only waiting on those files that has just arrived
            // var files = directoryInfoInputFolder.GetFiles().OrderByDescending(f => f.CreationTime).ToList();
            var files = fileInfos.OrderByDescending(f => f.CreationTime).ToList();

            if (this.Starting == false)
            {
                if (this.PollDelay > 0)
                {
                    if (this.PollDelay < 1000)
                    {
                        this.PollDelay = this.PollDelay * 1000;
                    }

                    Logger.InfoFormat("We are waiting {0} seconds before we start converting", this.PollDelay);
                    Thread.Sleep(this.PollDelay);
                }
            }

            this.CleanUpOldFiles(totalfiles, directoryInfoInputFolder);

            // List<string> xfiles = (from f in files select f.FullName).ToList();
            var fileUtilities = new FileUtilities();
            var xmlUtilities = new XmlUtilities();

            if (!files.Any())
            {
                return;
            }

            foreach (var file in files)
            {
                try
                {
                    // We check if the file has picture of not
                    var imageNodes = this.CheckIfFileHasPicture(file.FullName);
                    if (imageNodes.Count > 0)
                    {
                        // We are here if we have a picture
                        var pictureNames = this.GetPictureNames(imageNodes);

                        // we must check if the pictures are available
                        var counter = 0;

                        // This check might not be needed as we have moved the code for fetching files in directory up. 
                        // It could be that the pictures missing were in files just arriving in the directory
                        while (this.CheckIfPicturesAreAvailable(pictureNames) == false)
                        {
                            // We wait 10 seconds - just so we know that the file might be in place and might be in correct size
                            if (this.Starting == false)
                            {
                                Thread.Sleep(10 * 1000);
                            }

                            // We add to counter
                            counter++;

                            // If we hit 12, then we end the loop (this means we are waiting 120 seconds which is the same as two minutes
                            if (counter == 12)
                            {
                                break;
                            }
                        }
                    }

                    var cacheFiles = directoryInfoCacheFolder.GetFiles().ToList();
                    var sameVersion = false;

                    var documentId = xmlUtilities.GetDocumentId(file.FullName);

                    // If we cannot find a document Id, then we have a problem.
                    if (documentId == string.Empty)
                    {
                        Logger.Error("File does not contain document id, moving to error folder");
                        fileUtilities.MoveFileToFolder(file.FullName, this.DirErrorFolder);
                        continue;
                    }

                    var documentVersion = xmlUtilities.GetDocumentVersion(file.FullName);

                    // If we can't find a document version, then we have a problem
                    if (documentVersion == 0)
                    {
                        // We must move file to error folder (if file exists that is)
                        Logger.Error("File does not contain document version, moving to error folder");
                        fileUtilities.MoveFileToFolder(file.FullName, this.DirErrorFolder);
                        continue;
                    }

                    // Checking if we already have the file in the cache folder
                    var cacheFile = fileUtilities.FindFileInCacheByDocumentId(documentId, cacheFiles);

                    // If we have it, let's check if we have the same version
                    if (cacheFile)
                    {
                        // Check if what we have is a newer version
                        sameVersion = fileUtilities.FindFileVersionByVersionNumber(
                            documentVersion, 
                            documentId, 
                            cacheFiles);
                    }

                    // If we have the same version, we are to continue the loop
                    if (sameVersion)
                    {
                        Logger.Info("This is the same version. Moving file to done folder");

                        // We are continuing the loop
                        fileUtilities.MoveFileToFolder(file.FullName, this.DirDoneFolder);
                        continue;
                    }

                    // We are to create the cache-filename so we can delete it
                    var cacheFileToDelete = fileUtilities.CreatePreviousCacheFileName(documentId, documentVersion);

                    if (cacheFileToDelete != string.Empty)
                    {
                        // Creating the FileInfo Object for this job.
                        var completeCacheFileToDelete = this.DirCacheFolder + @"\" + cacheFileToDelete;
                        var fileInfoCacheFileToDelete = new FileInfo(completeCacheFileToDelete);

                        // We are deleting the file supposed to convert
                        fileUtilities.DeleteCacheFile(fileInfoCacheFileToDelete);
                    }

                    // We shall convert only if same version is false. 
                    this.ConvertFile(file.FullName, documentId, documentVersion);
                }
                catch (Exception exception)
                {
                    fileUtilities.MoveFileToFolder(file.FullName, this.DirErrorFolder);
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                }
            }

            // Only the latest X files!
            var maxCacheFiles =
                directoryInfoCacheFolder.GetFiles().OrderByDescending(f => f.CreationTime).Take(this.MaxFiles).ToList();

            if (this.OutputName == string.Empty)
            {
                throw new Exception("Output Name not defined. Exiting!");
            }

            var converter = new NewsConverter
                                          {
                                              XsltCombineFile = this.XsltFeedFile, 
                                              FeedPath = this.XmlFeedFolder, 
                                              OutputFileName = this.OutputName, 
                                              MaxFiles = this.MaxFiles, 
                                              MailReceivers = this.ErrorReceivers
                                          };

            // Combining the files
            converter.CombineFiles(maxCacheFiles);
        }

        /// <summary>
        /// The clean up old files.
        /// </summary>
        /// <param name="totalfiles">
        /// The total files.
        /// </param>
        /// <param name="directoryInfoInputFolder">
        /// The directory info input folder.
        /// </param>
        private void CleanUpOldFiles(int totalfiles, DirectoryInfo directoryInfoInputFolder)
        {
            // Clean up old files
            if (totalfiles > this.MaxFiles)
            {
                // Getting the number of files to delete
                int filesToDelete = this.FilesToDelete(totalfiles, this.MaxFiles);

                // Getting the actual files we are to delete
                List<FileInfo> oldFiles = this.DirectoryGetFiles(directoryInfoInputFolder, filesToDelete);

                // Loop over result
                foreach (FileInfo oldFile in oldFiles)
                {
                    // Delete files
                    Logger.Info("We are deleting: " + oldFile.Name);
                    File.Delete(oldFile.FullName);
                }
            }
        }

        /// <summary>
        /// The directory get files.
        /// </summary>
        /// <param name="directoryInfoInputFolder">
        /// The directory info input folder.
        /// </param>
        /// <param name="numberOfFilesToDelete">
        /// The number of files to delete.
        /// </param>
        /// <returns>
        /// The <see>
        ///         <cref>List</cref>
        ///     </see>
        ///     .
        /// </returns>
        public List<FileInfo> DirectoryGetFiles(DirectoryInfo directoryInfoInputFolder, int numberOfFilesToDelete)
        {
            return directoryInfoInputFolder.GetFiles().OrderBy(f => f.CreationTime).Take(numberOfFilesToDelete).ToList();
        }

        /// <summary>
        /// The files to delete.
        /// </summary>
        /// <param name="totalfiles">
        /// The totalfiles.
        /// </param>
        /// <param name="maxFiles">
        /// The max files.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        private int FilesToDelete(int totalfiles, int maxFiles)
        {
            // Let's say we have 100 files in the folder.
            // 30 files is the max number of files we are to keep
            // This means we are to delete 70 files
            return totalfiles - maxFiles;
        }

        /// <summary>
        /// Converting only one file
        /// </summary>
        /// <param name="filename">
        /// containing the name of the file we are to convert
        /// </param>
        /// <param name="documentId">
        /// containing the document Id in the XML file
        /// </param>
        /// <param name="documentVersion">
        /// containing the version number for this file
        /// </param>
        private void ConvertFile(string filename, string documentId, int documentVersion)
        {
            var newsConverter = new NewsConverter
                                              {
                                                  XmlFile = filename, 
                                                  XsltFile = this.XsltFile, 
                                                  OutPath = this.DirCacheFolder, 
                                                  DonePath = this.DirDoneFolder, 
                                                  ErrorPath = this.DirErrorFolder, 
                                                  XPathFilter = this.XPathExpression, 
                                                  DocumentId = documentId, 
                                                  DocumentVersion = documentVersion
                                              };
            newsConverter.ConvertFile();

            newsConverter.Dispose();
        }

        /// <summary>
        /// The file watcher_ created.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        /// <exception cref="Exception">
        /// An exception happens whenever an error occurs
        /// </exception>
        private void fileWatcher_Created(object sender, FileSystemEventArgs e)
        {
            try
            {
                this.busyEvent.WaitOne();

                ThreadContext.Properties["JOBNAME"] = this.InstanceName;
                Logger.InfoFormat("NewsComponent::fileWatcher_Created() hit");

                this.ConvertAndCombine();
            }
            catch (Exception exception)
            {
                Logger.Error("Something happened during fileWatcher_Created!");
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);

                // I would like to have an e-mail when this happens
                if (ConfigurationManager.AppSettings["SendErrorEmail"].ToUpper() != "TRUE")
                {
                    return;
                }

                MailAddress fromAddress = new MailAddress("505@ntb.no");
                MailAddress toAddress = new MailAddress(ConfigurationManager.AppSettings["EmailReceiver"]);
                MailMessage mailMessage = new MailMessage(fromAddress, toAddress)
                                              {
                                                  Subject =
                                                      "NTBNewsFeedService: Error when creating file", 
                                                  Body =
                                                      "An error has occured when creating message: "
                                                      + exception.Message + "\r\n"
                                                      + "Stack Trace: "
                                                      + exception.StackTrace
                                              };

                SmtpClient smtpClient = new SmtpClient { Host = ConfigurationManager.AppSettings["SMTPServer"] };

                smtpClient.Send(mailMessage);

                // Moving file to error folder
                var errorFile = this.DirErrorFolder + @"\" + e.Name;
                FileInfo fi = new FileInfo(errorFile);

                if (fi.Exists)
                {
                    fi.Delete();
                }

                File.Move(e.FullPath, this.DirErrorFolder + @"\" + e.Name);
            }
            finally
            {
                // Reset event
                this.busyEvent.Set();
            }
        }

        /// <summary>
        /// Handles the Error event of the filesWatcher control.
        /// </summary>
        /// <param name="sender">
        /// The source of the event.
        /// </param>
        /// <param name="e">
        /// The <see cref="System.IO.ErrorEventArgs"/> instance containing the event data.
        /// </param>
        /// <remarks>
        /// The error event is typically fired with FileSystemWatcher buffer overflows. This will cause fallback to <c>Continous</c> polling to avoid loosing files. 
        /// </remarks>
        private void filesWatcher_Error(object sender, ErrorEventArgs e)
        {
            // Log error
            ThreadContext.Properties["JOBNAME"] = this.InstanceName;
            Logger.ErrorFormat("NewsComponent::filesWatcher_Error() error encountered: {0}", e.GetException().Message);

            // Kill file event watcher and start retry loop
            this.fileWatcher.EnableRaisingEvents = false;
            this.ErrorRetry = true;
            this.pollTimer.Start();
        }

        /// <summary>
        /// The poll timer_ elapsed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        /// <exception cref="Exception">
        /// An exception occurs whenever there is an error
        /// </exception>
        private void pollTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            ThreadContext.Properties["JOBNAME"] = this.InstanceName;
            Logger.DebugFormat("NewsComponent::pollTimer_Elapsed() hit");

            // Reset flags and pollers
            this.busyEvent.WaitOne();
            this.pollTimer.Stop();

            try
            {
                this.ConvertAndCombine();

                // Logger.Debug("InputFolder for " + InstanceName + " : " + FileInputFolder);

                //// Process any wating files
                // List<string> files =
                // new List<string>(
                // Directory.GetFiles(
                // FileInputFolder,
                // FileFilter,
                // IncludeSubdirs ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly));

                //// Some logging
                // Logger.DebugFormat("NewsComponent::pollTimer_Elapsed() Convertion items scheduled: {0}", files.Count);

                // ConvertFiles(files);

                //// Only the latest 30 files!
                // DirectoryInfo directoryInfoCacheFolder = new DirectoryInfo(DirCacheFolder);

                // List<FileInfo> cacheFiles = directoryInfoCacheFolder.GetFiles().OrderByDescending(f => f.CreationTime).Take(maxFiles).ToList();

                // if (string.IsNullOrEmpty(OutputName))
                // {
                // throw new Exception("Outputname is empty. cannot create file");
                // }

                // NewsConverter converter = new NewsConverter
                // {
                // XsltCombineFile = XsltFeedFile,
                // FeedPath = XmlFeedFolder,
                // OutputFileName = OutputName,
                // maxFiles = maxFiles
                // };

                // converter.CombineFiles(cacheFiles);

                // Enable the event listening
                if (this.pollStyle == PollStyle.FileSystemWatch && !this.ErrorRetry)
                {
                    Logger.Debug("Setting filesWatcher.EnableRaisingEvents to true");
                    this.fileWatcher.EnableRaisingEvents = true;
                }

                if (this.pollStyle == PollStyle.Continous && !this.ErrorRetry)
                {
                    this.pollTimer.Start();
                }
            }
            catch (Exception exception)
            {
                Logger.Error("NewsComponent::pollTimer_Elapsed() error: " + exception.Message, exception);

                // I would like to have an e-mail when this happens
                if (ConfigurationManager.AppSettings["SendErrorEmail"].ToUpper() != "TRUE")
                {
                    return;
                }

                MailAddress fromAddress = new MailAddress("505@ntb.no");
                MailAddress toAddress = new MailAddress(ConfigurationManager.AppSettings["EmailReceiver"]);
                MailMessage mailMessage = new MailMessage(fromAddress, toAddress)
                                              {
                                                  Subject =
                                                      "NTBNewsFeedService: pollTimer_Elapsed() error", 
                                                  Body =
                                                      "An error has occured when creating message: "
                                                      + exception.Message + "\r\n"
                                                      + "Stack Trace: "
                                                      + exception.StackTrace
                                              };

                SmtpClient smtpClient = new SmtpClient { Host = ConfigurationManager.AppSettings["SMTPServer"] };
                smtpClient.Send(mailMessage);
            }

            // Check error state
            if (this.ErrorRetry)
            {
                this.fileWatcher.EnableRaisingEvents = false;
            }

            // Restart
            this.pollTimer.Interval = this.Interval * 1000;
            if (this.pollStyle != PollStyle.FileSystemWatch
                || (this.pollStyle == PollStyle.FileSystemWatch && this.fileWatcher.EnableRaisingEvents == false))
            {
                this.pollTimer.Start();
            }

            this.busyEvent.Set();
        }

        public string XmlPictureFolder { get; set; }

        /// <summary>
        ///     The starting 
        /// </summary>
        public bool Starting { get; set; }
    }
}