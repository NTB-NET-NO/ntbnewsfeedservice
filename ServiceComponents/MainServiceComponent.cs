﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MainServiceComponent.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   The main service component.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.NewsFeed.Components
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Configuration;
    using System.IO;
    using System.Linq;
    using System.ServiceModel;
    using System.Threading;
    using System.Timers;
    using System.Xml;

    // Add support for log4net
    using log4net;

    /// <summary>
    /// The main service component.
    /// </summary>
    public partial class MainServiceComponent : Component
    {
        /// <summary>
        /// Name of the file that contains the job configuration set
        /// </summary>
        /// <remarks>The name of the job config XML file is stored here.</remarks>
        protected string ConfigSet = string.Empty;

        /// <summary>
        /// Enable watching the config set for changes and reconfigure at runtime
        /// </summary>
        /// <remarks>config setting that defines if we are wathcing the config set for changes/auto reconfigure or not.</remarks>
        protected bool WatchConfigSet = true;

        /// <summary>
        /// List of currently configured jobs
        /// </summary>
        protected Dictionary<string, IBaseServiceComponent> JobInstances =
            new Dictionary<string, IBaseServiceComponent>();

        /// <summary>
        /// Push subscription job dictionary, maps exchange folder ids to job names
        /// </summary>
        protected Dictionary<string, string> PushSubscriptionJobs = new Dictionary<string, string>();

        /// <summary>
        /// List of folders to match events against
        /// </summary>
        protected List<string> PushSubscribeFolderIds = new List<string>();

        /// <summary>
        /// Notification handler service host
        /// </summary>
        protected ServiceHost ServiceHost = null;

        /// <summary>
        /// Notification handler service host
        /// </summary>
        protected Uri ServiceUrl = null;
        
        /// <summary>
        /// Static logger
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(MainServiceComponent));

        /// <summary>
        /// Initializes static members of the <see cref="MainServiceComponent"/> class. 
        /// </summary>
        static MainServiceComponent()
        {
            // Set up logger
            log4net.Config.XmlConfigurator.Configure();
            if (!LogManager.GetRepository().Configured)
            {
                log4net.Config.BasicConfigurator.Configure();
            }

            Logger = LogManager.GetLogger(typeof(MainServiceComponent));

            if (Thread.CurrentThread.Name == null)
            {
                Thread.CurrentThread.Name = "MainServiceComponent";
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MainServiceComponent"/> class.
        /// </summary>
        public MainServiceComponent()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MainServiceComponent"/> class.
        /// </summary>
        /// <param name="container">
        /// The container.
        /// </param>
        public MainServiceComponent(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        /// <summary>
        /// Configures this instance.
        /// </summary>
        public void Configure()
        {
            try
            {
                ThreadContext.Stacks["NDC"].Push("CONFIG");
                
                // Read params 
                ConfigSet = ConfigurationManager.AppSettings["ConfigurationSet"];
                WatchConfigSet = Convert.ToBoolean(ConfigurationManager.AppSettings["WatchConfiguration"]);
                
                // Logging
                Logger.InfoFormat("{0} : {1}", "ConfigurationSet", ConfigSet);
                Logger.InfoFormat("{0} : {1}", "WatchConfiguration", WatchConfigSet);

                // Load configuration set

                XmlDocument config = new XmlDocument();
                config.Load(ConfigSet);

                // Setting up the watcher
                configFileWatcher.Path = Path.GetDirectoryName(ConfigSet);
                configFileWatcher.Filter = Path.GetFileName(ConfigSet);

                // Clearing existing instances
                JobInstances.Clear();

                // Then create gatherers
                XmlNodeList nodes = config.SelectNodes("//NewsComponent[@Enabled='True']");
                
                if (nodes != null)
                {
                    int newsComponents = nodes.Count;
                    Logger.InfoFormat("NewsComponent job instances found: {0}", newsComponents);
                    foreach (XmlNode nd in nodes)
                    {
                        IBaseServiceComponent c = new NewsComponent();
                        c.Configure(nd);
                        JobInstances.Add(c.InstanceName, c);
                    }
                }

                nodes = config.SelectNodes("//JsonComponent[@Enabled='True']");
                if (nodes != null)
                {
                    int jsonComponents = nodes.Count;
                    Logger.InfoFormat("JsonComponent job instances found: {0}", jsonComponents);
                    foreach (XmlNode nd in nodes)
                    {
                        IBaseServiceComponent c = new JsonComponent();
                        c.Configure(nd);
                        JobInstances.Add(c.InstanceName, c);
                    }
                }
            }
            finally
            {
                ThreadContext.Stacks["NDC"].Pop();
            }
        }

        /// <summary>
        /// The pause.
        /// </summary>
        public void Pause()
        {
            maintenanceTimer.Stop();
            configFileWatcher.EnableRaisingEvents = false;

            // Stop instances
            foreach (KeyValuePair<string, IBaseServiceComponent> kvp in this.JobInstances.Where(kvp => kvp.Value.Enabled))
            {
                Logger.Info("Stopping " + kvp.Value);
                kvp.Value.Stop();
            }
        }

        /// <summary>
        /// Starts this instance.
        /// </summary>
        public void Start()
        {
            // Making sure the config-file is being watched
            if (configFileWatcher.EnableRaisingEvents == false)
            {
                configFileWatcher.EnableRaisingEvents = true;
            }

            NDC.Push("START");
            try
            {
                // Start instances
                foreach (KeyValuePair<string, IBaseServiceComponent> kvp in this.JobInstances.Where(kvp => kvp.Value.Enabled))
                {
                    kvp.Value.Start();
                    Logger.Info("Started: " + kvp.Value.InstanceName);
                }

                // Start maintenance
                maintenanceTimer.Start();
            }
            finally
            {
                NDC.Pop();
            }
        }

        /// <summary>
        /// Stops this instance.
        /// </summary>
        public void Stop()
        {
            // Kill the notification service handler
            if (ServiceHost != null)
            {
                ServiceHost.Close();
            }

            // Stop instances
            foreach (KeyValuePair<string, IBaseServiceComponent> kvp in JobInstances)
            {
                if (kvp.Value.Enabled)
                {
                    kvp.Value.Stop();
                }

                Logger.Info(kvp.Value.InstanceName + " stopped.");
            }
        }

        /// <summary>
        /// The maintenance timer_ elapsed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void maintenanceTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            MDC.Set("JOBNAME", "MainWorkerJob");

            // Logging what we are doing
            Logger.DebugFormat("MainServiceComponent::maintenanceTimer_Elapsed() hit");

            try
            {
                maintenanceTimer.Stop();

                /*
                 * I shall check if the jobs that are going really are working and running...
                 * 
                 */
                foreach (KeyValuePair<string, IBaseServiceComponent> kvp in JobInstances)
                {
                    if (!kvp.Value.Enabled)
                    {
                        continue;
                    }

                    if (kvp.Value.ComponentState != ComponentState.Halted)
                    {
                        continue;
                    }

                    Logger.Debug("The component is in Halted state, even though it is enabled!");
                    Logger.Debug("We are starting the component!");
                    kvp.Value.Start();
                }

                maintenanceTimer.Start();

                // Watch the config file
                if (!configFileWatcher.EnableRaisingEvents)
                {
                    configFileWatcher.EnableRaisingEvents = WatchConfigSet;
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat("MainServiceComponent::maintenanceTimer_Elapsed() failed: " + ex.Message, ex);
            }
        }

        /// <summary>
        /// The config file watcher_ changed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void configFileWatcher_Changed(object sender, FileSystemEventArgs e)
        {
            ThreadContext.Properties["JOBNAME"] = "MainServiceWorker";

            // Outer-try-final to prevent dupe events
            try
            {
                // Stopping file watcher
                configFileWatcher.EnableRaisingEvents = false;
                Logger.Info("MainServiceComponent::configFileWatcher_Changed() hit. Will reconfigure.");

                ThreadContext.Stacks["NDC"].Push("RECONFIGURE");

                // Pause everything
                Pause();

                // Give it a little break
                Thread.Sleep(5000);

                // Reconfigure
                ConfigurationManager.RefreshSection("applicationSettings");
                ConfigurationManager.RefreshSection("appSection");
                Configure();

                Start();
            }
            catch (Exception ex)
            {
                Logger.Fatal("NTBNewsFeedService reconfiguration failed - TERMINATING!", ex);
                throw;
            }
            finally
            {
                ThreadContext.Stacks["NDC"].Pop();
                configFileWatcher.EnableRaisingEvents = WatchConfigSet;
            }
        }
    }
}