﻿using System;

namespace NTB.NewsFeed.Components
{
    partial class NewsComponent
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.fileWatcher = new System.IO.FileSystemWatcher();
            this.pollTimer = new System.Timers.Timer();
            ((System.ComponentModel.ISupportInitialize)(this.fileWatcher)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pollTimer)).BeginInit();
            // 
            // fileWatcher
            // 
            this.fileWatcher.EnableRaisingEvents = true;
            this.fileWatcher.Created += new System.IO.FileSystemEventHandler(this.fileWatcher_Created);
            this.fileWatcher.Error += new System.IO.ErrorEventHandler(filesWatcher_Error);
            ((System.ComponentModel.ISupportInitialize)(this.pollTimer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fileWatcher)).EndInit();
            // 
            // pollTimer
            // 
            this.pollTimer.Enabled = true;
            this.pollTimer.Elapsed += new System.Timers.ElapsedEventHandler(this.pollTimer_Elapsed);
            ((System.ComponentModel.ISupportInitialize)(this.fileWatcher)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pollTimer)).EndInit();

        }

        #endregion

        private System.IO.FileSystemWatcher fileWatcher;
        private System.Timers.Timer pollTimer;

        /// <summary>
        /// Flag to tell which state the component is in (running/halted)
        /// </summary>
        protected ComponentState componentState;

        /// <summary>
        /// The enabled status for this instance
        /// </summary>
        /// <remarks>Internal field, accessed through interface implemenation <see cref="Enabled"/></remarks>
        protected bool enabled;

        /// <summary>
        /// The polling style for this instance
        /// </summary>
        /// <remarks>Internal field, accessed through interface implemenation <see cref="PollStyle"/></remarks>
        protected PollStyle pollStyle;
    }
}
