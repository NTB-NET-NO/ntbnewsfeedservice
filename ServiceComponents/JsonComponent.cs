﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="JsonComponent.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   Defines the NewsComponent type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.NewsFeed.Components
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Configuration;
    using System.IO;
    using System.Linq;
    using System.Net.Mail;
    using System.Threading;
    using System.Xml;

    // Adding support for log4net
    using log4net;

    using Utilities;

    /// <summary>
    /// The news component.
    /// </summary>
    public partial class JsonComponent : Component, IBaseServiceComponent
    {
        /// <summary>
        /// Static logger
        /// </summary>
        protected static readonly ILog Logger = LogManager.GetLogger(typeof(MainServiceComponent));

        /// <summary>
        /// The configured job name for this instance
        /// </summary>
        /// <remarks>Internal field, accessed through interface implemenation <see cref="InstanceName"/></remarks>
        protected string Name;

        /// <summary>
        /// Flag to indicate sucessful configure. Instance will not start polling nor handle messages if not properly configured
        /// </summary>
        /// <remarks>Holds the configured status of the instance. <c>True</c> means successfully configured</remarks>
        protected bool Configured = false;

        /// <summary>
        /// Error-Retry flag
        /// </summary>
        /// <remarks>The flag is being set if a network or EWS error is encountered. Current operations are being aborted for later retry.</remarks>
        protected bool ErrorRetry = false;

        /// <summary>
        /// PollDelay is used to make sure that the file is ready to be accessed. 
        /// </summary>
        protected int PollDelay = 0;

        /// <summary>
        /// Send Email-notifications when a new messages is processed
        /// </summary>
        /// <remarks>Set to an email address. Multiple adresses are supported, separate with <c>;</c></remarks>
        protected string EmailNotification;

        /// <summary>
        /// Subject for email notifications
        /// </summary>
        /// <remarks>The subject is built during processing and used when sending the email when processing completes</remarks>
        protected string EmailSubject;

        /// <summary>
        /// Body for email notifications
        /// </summary>
        /// <remarks>The body is built during processing and used when sending the email when processing completes</remarks>
        protected string EmailBody;

        /// <summary>
        /// The configured schedule for this instance
        /// </summary>
        /// <remarks>This is set to a time of day and is used by the <c>Scheduled</c> pollstyle. Work is triggered at the given time of day.</remarks>
        protected string Schedule;

        /// <summary>
        /// The configured interval for this instance, used for Continous polling
        /// </summary>
        /// <remarks>
        ///   <para>Indicates the interval time in seconds for <c>Continous</c>polling. 60 means that the job runs every minute.</para>
        ///   <para>Default value: <c>60</c></para>
        /// </remarks>
        protected int Interval = 60;

        /// <summary>
        /// Input file folder
        /// </summary>
        /// <remarks>
        /// File folder where incoming files are read from. Inputs can be modified by <see cref="FileFilter"/> and <see cref="IncludeSubdirs"/>
        /// </remarks>
        protected string FileInputFolder;

        /// <summary>
        /// Output file folder
        /// </summary>
        /// <remarks>
        /// File folder where incoming files are read from. Inputs can be modified by <see cref="FileFilter"/> and <see cref="IncludeSubdirs"/>
        /// </remarks>
        protected Dictionary<int, string> FileOutputFolders = new Dictionary<int, string>();

        /// <summary>
        /// The file xslt folder.
        /// </summary>
        protected string FileXsltFolder;

        /// <summary>
        /// Error file folder
        /// </summary>
        /// <remarks>
        ///   <para>File folder where failing files are stored.</para>
        ///   <para>If this is not set, failing files are deleted instead of saved.</para>
        /// </remarks>
        protected string FileErrorFolder;

        /// <summary>
        /// File folder where completed files are stored
        /// </summary>
        /// <remarks>
        ///   <para>File folder where completed files are archived. Subdirectories for years, months and dates are created.</para>
        ///   <para>If this is not set, imported files are deleted instead of archived.</para>
        /// </remarks>
        protected string FileDoneFolder;

        /// <summary>
        /// Input file filter
        /// </summary>
        /// <remarks>
        ///   <para>Defines what file types are being read from <see cref="FileInputFolder"/>.</para>
        ///   <para>Default value: <c>NTBIDType.None</c></para>
        /// </remarks>
        protected string FileFilter = "*.xml";

        /// <summary>
        /// Watch subdirs for new files as well
        /// </summary>
        /// <remarks>
        ///   <para>Defines if the instance should look in subdirectories within <see cref="FileInputFolder"/>.</para>
        ///   <para>Default value: <c>False</c></para>
        /// </remarks>
        protected bool IncludeSubdirs = false;

        /// <summary>
        /// Buffer size for the file watcher
        /// </summary>
        /// <remarks>
        ///   <para>Defines the internal buffer size the FileSystemWatcher when using this polling.
        /// Should not be set to high. The instance will fall back to contious polling on buffer overflows.</para>
        ///   <para>Default value: <c>4096</c></para>
        /// </remarks>
        protected int BufferSize = 4096;

        /// <summary>
        /// Flag to say if this component is working or not. I have a feeling we have another parameter for this though.
        /// </summary>
        protected bool BusyFlag = false;

        /// <summary>
        /// Dictionary to map ID to encodings, which we can use to say which type of encoding the output file shall have
        /// </summary>
        protected Dictionary<int, string> DocumentEncodings = new Dictionary<int, string>();

        /// <summary>
        /// Dictionary to map ID to file extensions, which we can use to say which type of encoding the output file shall have
        /// </summary>
        protected Dictionary<int, string> DocumentExtensions = new Dictionary<int, string>();

        /// <summary>
        /// Busy status event
        /// </summary>
        private readonly AutoResetEvent _busyEvent = new AutoResetEvent(true);

        /// <summary>
        /// Exit control event
        /// </summary>
        private readonly AutoResetEvent _stopEvent = new AutoResetEvent(false);

        /// <summary>
        /// Gets or sets the Error Receivers List of Email receivers
        /// </summary>
        private List<string> ErrorReceivers { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="JsonComponent"/> class.
        /// </summary>
        public JsonComponent()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="JsonComponent"/> class.
        /// </summary>
        /// <param name="container">
        /// The container.
        /// </param>
        public JsonComponent(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        /// <summary>
        /// Gets a value indicating whether enabled.
        /// </summary>
        public bool Enabled
        {
            get { return enabled; }
        }

        /// <summary>
        /// Gets the instance name.
        /// </summary>
        public string InstanceName
        {
            get { return Name; }
        }

        /// <summary>
        /// Gets the operation mode.
        /// </summary>
        public OperationMode OperationMode
        {
            get { return OperationMode.Converter; }
        }

        /// <summary>
        /// Gets the poll style.
        /// </summary>
        public PollStyle PollStyle
        {
            get { return pollStyle; }
        }

        /// <summary>
        /// Gets or sets the dir cache folder.
        /// </summary>
        public string DirCacheFolder { get; set; }

        /// <summary>
        /// Gets or sets the dir done folder.
        /// </summary>
        public string DirDoneFolder { get; set; }

        /// <summary>
        /// Gets or sets the dir error folder.
        /// </summary>
        public string DirErrorFolder { get; set; }

        /// <summary>
        /// Gets or sets the max files.
        /// </summary>
        public int MaxFiles { get; set; }

        /// <summary>
        /// Gets or sets the xslt feed file.
        /// </summary>
        public string XsltFeedFile { get; set; }

        /// <summary>
        /// Gets or sets the xml feed folder.
        /// </summary>
        public string JsonFeedFolder { get; set; }

        /// <summary>
        /// Gets or sets the output name.
        /// </summary>
        public string OutputName { get; set; }

        /// <summary>
        /// Gets the component state.
        /// </summary>
        public ComponentState ComponentState
        {
            get { return componentState; }
        }

        /// <summary>
        /// Gets or sets the x path expression.
        /// </summary>
        protected string XPathExpression { get; set; }
        
        /// <summary>
        /// The configure.
        /// </summary>
        /// <param name="configNode">
        /// The config node.
        /// </param>
        public void Configure(XmlNode configNode)
        {
            Logger.Debug("Node: " + configNode.Name);
            if (configNode.Attributes != null)
            {
                Logger.Debug("Name attribut: " + configNode.Attributes.GetNamedItem("Name"));

                // Basic configuration sanity check
                if (configNode.Name != "JsonComponent" ||
                    configNode.Attributes.GetNamedItem("Name") == null)
                {
                    throw new ArgumentException("The XML configuration node passed is invalid", "configNode");
                }

                #region Basic config

                // Getting the name of this Component instance
                try
                {
                    Name = GetInstanceNameValue(configNode);
                    enabled = GetComponentEnabledValue(configNode);
                    ThreadContext.Stacks["NDC"].Push(InstanceName);
                    
                    BufferSize = GetBufferSizeValue(configNode);

                    MaxFiles = GetMaxFilesValue(configNode);

                    OutputName = GetOutputNameValue(configNode);

                    PollDelay = GetPollDelayValue(configNode);

                    if (configNode.Attributes["ErrorNotification"] != null)
                    {
                        ErrorReceivers = configNode.Attributes["ErrorNotification"].Value.Split(',').ToList();
                    }
                }
                catch (Exception ex)
                {
                    Logger.Fatal("Not possible to configure this job instance!", ex);
                }

                try
                {
                    PollStyle? pollStyleValue = GetPollStyleValue(configNode);
                    if (pollStyleValue != null)
                    {
                        pollStyle = (PollStyle) pollStyleValue;
                    }
                }
                catch (Exception ex)
                {
                    ThreadContext.Stacks["NDC"].Pop();
                    throw new ArgumentException("Invalid or missing OperationMode values in XML configuration node", ex);
                }
            }

            #endregion

            #region Folder generation
            try
            {
                JsonFeedFolder = GetJsonFeedFolder(configNode);

                Logger.InfoFormat(
                    "Converter job - Polling: {0} / XMLFeed Folder: {1} / Enabled: {2}",
                    Enum.GetName(typeof(PollStyle), pollStyle),
                    JsonFeedFolder,
                    enabled);

            }
            catch (Exception ex)
            {
                Logger.Fatal("Not possible to get XMLFeed Folder", ex);
            }

            try
            {
                DirErrorFolder = GetJsonErrorFolder(configNode);

                Logger.InfoFormat(
                    "Converter job - Polling: {0} / Directory Cache folder: {1} / Enabled: {2}",
                    Enum.GetName(typeof(PollStyle), pollStyle),
                    DirErrorFolder,
                    enabled);
            }
            catch (Exception ex)
            {
                Logger.Fatal("Not possible to get Error-folder", ex);
            }

            try
            {
                DirDoneFolder = GetJsonDoneFolderValue(configNode);

                Logger.InfoFormat(
                    "Converter job - Polling: {0} / Directory Cache folder: {1} / Enabled: {2}",
                    Enum.GetName(typeof(PollStyle), pollStyle),
                    DirDoneFolder,
                    enabled);
            }
            catch (Exception ex)
            {
                Logger.Fatal("Not possible to get Directory Cache-folder", ex);
            }

            try
            {
                FileInputFolder = GetJsonInputFolderValue(configNode);

                Logger.InfoFormat(
                    "Converter job - Polling: {0} / File input folder: {1} / Enabled: {2}",
                    Enum.GetName(typeof(PollStyle), pollStyle),
                    FileInputFolder,
                    enabled);
            }
            catch (Exception ex)
            {
                Logger.Fatal("Not possible to get Json-Input-folder", ex);
            }

            #endregion

            // This is where the FileFolder creation shall end
            if (enabled)
            {
                #region File folders to access
                // Checking if file folders exists
                string fileOutputFolder = string.Empty;

                try
                {
                    if (!Directory.Exists(FileInputFolder))
                    {
                        Directory.CreateDirectory(FileInputFolder);
                    }

                    foreach (KeyValuePair<int, string> kvp in FileOutputFolders)
                    {
                        fileOutputFolder = kvp.Value;
                        if (!Directory.Exists(fileOutputFolder))
                        {
                            Directory.CreateDirectory(fileOutputFolder);
                        }
                    }
                }
                catch (Exception ex)
                {
                    ThreadContext.Stacks["NDC"].Pop();
                    throw new ArgumentException("Invalid, unknown or missing file folder: " + fileOutputFolder, ex);
                }

                #endregion

                #region Set up polling
                try
                {
                    CheckPollStyleValue(configNode);
                }
                catch (Exception ex)
                {
                    ThreadContext.Stacks["NDC"].Pop();
                    throw new ArgumentException("Invalid or missing PollStyle-specific values in XML configuration node: " + ex.Message, ex);
                }
                #endregion
            }

            // Finish configuration
            ThreadContext.Stacks["NDC"].Pop();
            Configured = true;
        }

        private void CheckPollStyleValue(XmlNode configNode)
        {
            // Switch on pollstyle
            switch (pollStyle)
            {
                case PollStyle.Continous:
                    Interval = Convert.ToInt32(configNode.Attributes["Interval"].Value)*1000;
                    if (Interval == 0)
                    {
                        Interval = 5000;
                    }

                    pollTimer.Interval = Interval; // short startup - interval * 1000;
                    Logger.DebugFormat("Poll interval: {0} seconds", Interval);

                    break;

                case PollStyle.Scheduled:
                    /* 
                            schedule = configNode.Attributes["Schedule"].Value;
                            TimeSpan s = Utilities.GetScheduleInterval(schedule);
                            logger.DebugFormat("Schedule: {0} Calculated time to next run: {1}", schedule, s);
                            pollTimer.Interval = s.TotalMilliseconds;
                            */
                    throw new NotSupportedException("Invalid polling style for this job type");

                case PollStyle.FileSystemWatch:
                    // Check for config overrides
                    if (configNode.Attributes.GetNamedItem("BufferSize") != null)
                    {
                        BufferSize = Convert.ToInt32(configNode.Attributes["BufferSize"].Value);
                    }

                    Logger.DebugFormat("FileSystemWatcher buffer size: {0}", BufferSize);

                    // Set values
                    fileWatcher.Path = FileInputFolder;
                    fileWatcher.Filter = FileFilter;
                    fileWatcher.IncludeSubdirectories = IncludeSubdirs;
                    fileWatcher.InternalBufferSize = BufferSize;

                    // Do not start the event watcher here, wait until after the startup cleaning job
                    pollTimer.Interval = 5000; // short startup;

                    break;

                default:
                    // Unsupported pollstyle for this object
                    throw new NotSupportedException("Invalid polling style for this job type");
            }
        }

        private string GetJsonInputFolderValue(XmlNode configNode)
        {
            string jsonFileInputFolder = string.Empty;
            // Find the file folder to work with
            XmlNode folderNode = configNode.SelectSingleNode("JsonInputFolder");

            if (folderNode != null)
            {
                jsonFileInputFolder = folderNode.InnerText;
            }

            return jsonFileInputFolder;
        }

        private string GetJsonDoneFolderValue(XmlNode configNode)
        {
            string jsonDoneFolder = string.Empty;
            // Find the file folder to work with
            XmlNode folderNode = configNode.SelectSingleNode("JsonDoneFolder");

            // configNode.SelectSingleNode("Folder").Attributes("XMLOutputFolder");
            if (folderNode != null)
            {
                jsonDoneFolder = folderNode.InnerText;

                if (!Directory.Exists(jsonDoneFolder))
                {
                    Directory.CreateDirectory(jsonDoneFolder);
                }
            }

            return jsonDoneFolder;
        }

        private string GetJsonErrorFolder(XmlNode configNode)
        {
            // Find the file folder to work with
            XmlNode folderNode = configNode.SelectSingleNode("JsonErrorFolder");
            string jsonErrorFolder = string.Empty;
            // configNode.SelectSingleNode("Folder").Attributes("XMLOutputFolder");
            if (folderNode != null)
            {
                jsonErrorFolder = folderNode.InnerText;

                if (!Directory.Exists(jsonErrorFolder))
                {
                    Directory.CreateDirectory(jsonErrorFolder);
                }
            }

            return jsonErrorFolder;
        }

        private string GetJsonFeedFolder(XmlNode configNode)
        {
            // Find the file folder to work with
            XmlNode folderNode = configNode.SelectSingleNode("JsonFeedFolder");
            string jsonFeedFolder = string.Empty;
            if (folderNode != null)
            {
                jsonFeedFolder = folderNode.InnerText;

                if (!Directory.Exists(jsonFeedFolder))
                {
                    Directory.CreateDirectory(jsonFeedFolder);
                }
            }

            return jsonFeedFolder;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="configNode"></param>
        /// <returns></returns>
        private PollStyle? GetPollStyleValue(XmlNode configNode)
        {
            if (configNode.Attributes != null)
            {
                return (PollStyle) Enum.Parse(typeof (PollStyle), configNode.Attributes["PollStyle"].Value, true);
            }

            return null;
        }

        /// <summary>
        ///     Getting the Poll Delay value from the XML File
        /// </summary>
        /// <param name="configNode">The node for this specific component</param>
        /// <returns>integer value stating the delay between each poll</returns>
        private int GetPollDelayValue(XmlNode configNode)
        {
            int pollDelay = 0;
            
            if (configNode.Attributes != null && configNode.Attributes["Pause"] != null)
            {
                pollDelay = Convert.ToInt32(configNode.Attributes["Pause"].Value);

                if (pollDelay < 1000)
                {
                    pollDelay = pollDelay*1000;
                }
            }

            // Returning value
            return pollDelay;
        }

        /// <summary>
        /// Method to get the outputname value from the XML
        /// </summary>
        /// <param name="configNode">Xml Node containing information regarding this component</param>
        /// <returns>string containing the name of output</returns>
        private string GetOutputNameValue(XmlNode configNode)
        {
            string outputName = string.Empty;
            if (configNode.Attributes != null && configNode.Attributes["OutputName"] != null)
            {
                outputName = configNode.Attributes["OutputName"].Value;
            }

            return string.IsNullOrEmpty(outputName) ? ConfigurationManager.AppSettings["DefaultFeedName"] : outputName;
        }

        private int GetBufferSizeValue(XmlNode configNode)
        {
            return configNode.Attributes != null ? Convert.ToInt32(configNode.Attributes["BufferSize"].Value) : 0;
        }

        private Boolean GetComponentEnabledValue(XmlNode configNode)
        {
            return configNode.Attributes != null && Convert.ToBoolean(configNode.Attributes["Enabled"].Value);
        }

        private string GetInstanceNameValue(XmlNode configNode)
        {
            return configNode.Attributes != null ? configNode.Attributes["Name"].Value : null;
        }

        private int GetMaxFilesValue(XmlNode configNode)
        {
            int numberOfFiles = 0;

            if (configNode.Attributes != null)
            {
                numberOfFiles = Convert.ToInt32(configNode.Attributes["MaxFiles"].Value);
            }

            if (numberOfFiles == 0)
            {
                numberOfFiles = Convert.ToInt32(ConfigurationManager.AppSettings["DefaultMaxFiles"]);
            }
            return numberOfFiles;
        }

        /// <summary>
        /// The start.
        /// </summary>
        /// <exception cref="NewsFeedsException">
        /// A News Feed Exception occurs if there is an error here
        /// </exception>
        /// <exception cref="Exception">
        /// An Exception occurs if there is an error here
        /// </exception>
        public void Start()
        {
            if (!Configured)
            {
                throw new NewsFeedsException("JsonComponent is not properly configured. NewsComponent::Start() Aborted!");
            }

            if (!enabled)
            {
                throw new NewsFeedsException("JsonComponent is not enabled. NewsComponent::Start() Aborted!");
            }

            Logger.Info("In Start() " + InstanceName + ".");

            // I first want to check input folder and see if there are files in there. If there are, they are to be converted
            DirectoryInfo directoryInfoInputFolder = new DirectoryInfo(FileInputFolder);
            
            // We don't need to loop much, as we are to only deliver one file
            List<FileInfo> files = directoryInfoInputFolder.GetFiles().OrderByDescending(f => f.CreationTime).ToList();

            string jsonString = string.Empty;
            foreach (JsonConverter jsonConverter in files.Select(fileInfo => new JsonConverter
                {
                    XmlFile = fileInfo.FullName,
                    DonePath = DirDoneFolder,
                    ErrorPath = DirErrorFolder,
                    FeedPath = JsonFeedFolder,
                    OutputFileName = OutputName,
                    MailReceivers = ErrorReceivers
                }))
            {
                jsonString = jsonConverter.ConvertFile();
            }

            if (jsonString != string.Empty)
            {
                WriteJson(jsonString);
            }

            // Now deleting the feed file
            foreach (FileInfo file in files)
            {
                // moving the file
                Logger.Info("moving file: " + file + " to " + DirDoneFolder);
                FileInfo fileInfo = new FileInfo(file.FullName);
                string filename = fileInfo.Name;

                FileInfo moveFile = new FileInfo(DirDoneFolder + @"\" + filename);
                if (moveFile.Exists)
                {
                    moveFile.Delete();
                }
                
                fileInfo.MoveTo(DirDoneFolder + @"\" + filename);
            }
            _stopEvent.Reset();

            _busyEvent.Set();

            componentState = ComponentState.Running;

            pollTimer.Start();
        }

        /// <summary>
        /// The stop.
        /// </summary>
        /// <exception cref="NewsFeedsException">
        /// A News Feed Exception occurs if there is an error here
        /// </exception>
        public void Stop()
        {
            if (!Configured)
            {
                throw new NewsFeedsException("JsonComponent is not properly configured. NFFComponent::Stop() Aborted");
            }

            if (!enabled)
            {
                throw new NewsFeedsException("JsonComponent is not properly configured. NFFComponent::Stop() Aborted");
            }

            const double epsilon = 0;
            if (!pollTimer.Enabled && (ErrorRetry || pollStyle !=
                PollStyle.FileSystemWatch || Math.Abs(pollTimer.Interval - 5000) < epsilon))
            {
                Logger.InfoFormat("Waiting for instance to complete work. Job {0}", InstanceName);
            }

            // Signal Stop
            _stopEvent.Set();

            // Stop events
            fileWatcher.EnableRaisingEvents = false;

            if (!_busyEvent.WaitOne(30000))
            {
                Logger.InfoFormat("Instance did not complete properly. Data may be inconsistent. Job: {0}", InstanceName);
            }

            componentState = ComponentState.Halted;
            
            // Kill polling
            pollTimer.Stop();
        }
        /// <summary>
        /// The file_error method
        /// </summary>
        /// <param name="sender">The sender</param>
        /// <param name="e">The event error parameters</param>
        void fileWatcher_Error(object sender, ErrorEventArgs e)
        {
            // Log error
            ThreadContext.Properties["JOBNAME"] = InstanceName;
            Logger.ErrorFormat("JsonComponent::filesWatcher_Error() error encountered: {0}", e.GetException().Message);

            // Kill file event watcher and start retry loop
            fileWatcher.EnableRaisingEvents = false;
            ErrorRetry = true;
            pollTimer.Start();
        }

        /// <summary>
        /// The file watcher_ created.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        /// <exception cref="Exception">
        /// An exception happens whenever an error occurs
        /// </exception>
        private void fileWatcher_Created(object sender, FileSystemEventArgs e)
        {
            try
            {
                ThreadContext.Properties["JOBNAME"] = InstanceName;
                Logger.InfoFormat("JsonComponent::fileWatcher_Created() hit");

                if (PollDelay > 0)
                {
                    if (PollDelay < 1000)
                    {
                        PollDelay = PollDelay * 1000;
                    }
                    Logger.InfoFormat("We are waiting {0} seconds before we start convertig", PollDelay);
                    Thread.Sleep(PollDelay);
                }

                // Wait for the busy state
                _busyEvent.WaitOne();

                List<FileInfo> files = new List<FileInfo> { new FileInfo(e.FullPath) };
                foreach (FileInfo file in files)
                {
                    if (file.Exists)
                    {
                        JsonConverter jsonConverter = new JsonConverter
                                {
                                    XmlFile = file.FullName,
                                    DonePath = DirDoneFolder,
                                    ErrorPath = DirErrorFolder,
                                    OutputFileName = OutputName,
                                    FeedPath = JsonFeedFolder,
                                    MailReceivers = ErrorReceivers
                                };

                        string jsonString = jsonConverter.ConvertFile();

                        jsonConverter.Dispose();

                        WriteJson(jsonString);

                        // moving the file (Consider creating this code as a separate method)
                        Logger.Info("moving file: " + file + " to " + DirDoneFolder);
                        FileInfo fileInfo = new FileInfo(file.FullName);
                        string filename = fileInfo.Name;
                        FileInfo moveFile = new FileInfo(DirDoneFolder + @"\" + filename);
                        if (moveFile.Exists)
                        {
                            moveFile.Delete();
                        }
                
                        fileInfo.MoveTo(DirDoneFolder + @"\" + filename);
                    }
                }

                // Reset event
                _busyEvent.Set();
            }
            catch (Exception exception)
            {

                // Sending an e-mail in case something went wrong.
                Logger.Error("Something happened during fileWatcher_Created!");
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);

                // I would like to have an e-mail when this happens
                if (ConfigurationManager.AppSettings["SendErrorEmail"].ToUpper() != "TRUE")
                {
                    return;
                }

                MailAddress fromAddress = new MailAddress("505@ntb.no");
                MailAddress toAddress = new MailAddress(ConfigurationManager.AppSettings["EmailReceiver"]);
                MailMessage mailMessage = new MailMessage(fromAddress, toAddress)
                    {
                        Subject = "NTBNewsFeedService: Error when creating file",
                        Body = "An error has occured when creating message: " + exception.Message + "\r\n" +
                               "Stack Trace: " + exception.StackTrace
                    };

                SmtpClient smtpClient = new SmtpClient { Host = ConfigurationManager.AppSettings["SMTPServer"] };

                smtpClient.Send(mailMessage);
            }
        }

        /// <summary>
        /// The poll timer_ elapsed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        /// <exception cref="Exception">
        /// An exception occurs whenever there is an error
        /// </exception>
        private void pollTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            ThreadContext.Properties["JOBNAME"] = InstanceName;
            Logger.DebugFormat("JsonComponent::pollTimer_Elapsed() hit");

            // Reset flags and pollers
            _busyEvent.WaitOne();
            pollTimer.Stop();

            try
            {
                Logger.Debug("InputFolder for " + InstanceName + " : " + FileInputFolder);

                // Process any wating files
                List<string> files =
                    new List<string>(
                        Directory.GetFiles(
                            FileInputFolder,
                            FileFilter,
                            IncludeSubdirs ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly));

                // Some logging
                Logger.DebugFormat("JsonComponent::pollTimer_Elapsed() Convertion items scheduled: {0}", files.Count);

                if (!files.Any())
                {
                    Logger.Info("No files to process");

                    // Making sure we are starting up the timer again
                    if (pollStyle == PollStyle.Continous && !ErrorRetry)
                    {
                        pollTimer.Start();
                    }

                    // Setting the busy event again.
                    _busyEvent.Set();
                    return;
                }

                foreach (string stringFile in files)
                {
                    // Process the files
                    Logger.Debug("Process the files to JSON: " + stringFile);
                    JsonConverter jsonConverter = new JsonConverter
                        {
                            XmlFile = stringFile,
                            DonePath = DirDoneFolder,
                            ErrorPath = DirErrorFolder,
                            OutputFileName = OutputName,
                            FeedPath = JsonFeedFolder,
                            MailReceivers = ErrorReceivers
                        };
                    string jsonContent = jsonConverter.ConvertFile();
                    WriteJson(jsonContent);

                    // moving the file 
                    // Getting the filename
                    FileInfo fileInfo = new FileInfo(stringFile);
                    string filename = fileInfo.Name;

                    // done: There is an error here. check what format the stringFile really is in
                    Logger.Info("moving file: " + stringFile + " to " + DirDoneFolder + @"\" + filename);
                    FileInfo moveFile = new FileInfo(DirDoneFolder + @"\" + filename);
                    if (moveFile.Exists)
                    {
                        moveFile.Delete();
                    }
                        
                    fileInfo.MoveTo(DirDoneFolder + @"\" + filename);
                }
                

                // Enable the event listening
                Logger.Debug("Pollstyle is: " + pollStyle);
                if (pollStyle == PollStyle.FileSystemWatch && !ErrorRetry)
                {
                    Logger.Debug("Setting filesWatcher.EnableRaisingEvents to true");
                    fileWatcher.EnableRaisingEvents = true;
                }

                if (pollStyle == PollStyle.Continous && !ErrorRetry)
                {
                    pollTimer.Start();
                }
            }
            catch (Exception exception)
            {
                Logger.Error("JsonComponent::pollTimer_Elapsed() error: " + exception.Message, exception);

                // I would like to have an e-mail when this happens
                if (ConfigurationManager.AppSettings["SendErrorEmail"].ToUpper() != "TRUE")
                {
                    return;
                }

                MailAddress fromAddress = new MailAddress("505@ntb.no");
                MailAddress toAddress = new MailAddress(ConfigurationManager.AppSettings["EmailReceiver"]);
                MailMessage mailMessage = new MailMessage(fromAddress, toAddress)
                    {
                        Subject = "NTBNewsFeedService: pollTimer_Elapsed() error",
                        Body = "An error has occured when creating message: " + exception.Message + "\r\n" +
                               "Stack Trace: " + exception.StackTrace
                    };

                SmtpClient smtpClient = new SmtpClient { Host = ConfigurationManager.AppSettings["SMTPServer"] };
                smtpClient.Send(mailMessage);
            }

            // Check error state
            if (ErrorRetry)
            {
                fileWatcher.EnableRaisingEvents = false;

                // Restart
                pollTimer.Interval = Interval*1000;
                if (pollStyle == PollStyle.FileSystemWatch && fileWatcher.EnableRaisingEvents == false)
                {
                    pollTimer.Start();
                }
            }

            _busyEvent.Set();
        }

        /// <summary>
        /// The write json.
        /// </summary>
        /// <param name="jsonString">
        /// The json string.
        /// </param>
        private void WriteJson(string jsonString)
        {
            try
            {
                FileInfo fileInfo = new FileInfo(JsonFeedFolder + @"\" + OutputName);

                if (fileInfo.Exists)
                {
                    fileInfo.Delete();
                }

                File.WriteAllText(JsonFeedFolder + @"\" + OutputName, jsonString);

                string message = "JSON fil skrevet til : "
                                 + Environment.NewLine
                                 + JsonFeedFolder + @"\" + OutputName + Environment.NewLine;
                try
                {
                    if (ErrorReceivers.Count > 0)
                    {
                        Mail.SendMessage(message, ErrorReceivers);
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                    if (exception.InnerException == null) return;
                    Logger.Error(exception.InnerException.Message);
                    Logger.Error(exception.InnerException.StackTrace);
                }
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);

                if (exception.InnerException == null) return;
                Logger.Error(exception.InnerException.Message);
                Logger.Error(exception.InnerException.StackTrace);
            }
        }

        private void fileWatcher_Changed(object sender, FileSystemEventArgs e)
        {
            try
            {
                ThreadContext.Properties["JOBNAME"] = InstanceName;
                Logger.InfoFormat("JsonComponent::fileWatcher_Created() hit");

                if (PollDelay > 0)
                {
                    if (PollDelay < 1000)
                    {
                        PollDelay = PollDelay * 1000;
                    }
                    Logger.InfoFormat("We are waiting {0} seconds before we start converting", PollDelay);
                    Thread.Sleep(PollDelay);
                }

                // Wait for the busy state
                _busyEvent.WaitOne();

                List<FileInfo> files = new List<FileInfo> { new FileInfo(e.FullPath) };
                foreach (FileInfo file in files)
                {
                    if (file.Exists)
                    {
                        JsonConverter jsonConverter = new JsonConverter
                        {
                            XmlFile = file.FullName,
                            DonePath = DirDoneFolder,
                            ErrorPath = DirErrorFolder,
                            OutputFileName = OutputName,
                            FeedPath = JsonFeedFolder,
                            MailReceivers = ErrorReceivers
                        };

                        string jsonString = jsonConverter.ConvertFile();

                        jsonConverter.Dispose();

                        WriteJson(jsonString);

                        // moving the file (Consider creating this code as a separate method)
                        Logger.Info("moving file: " + file + " to " + DirDoneFolder);
                        FileInfo fileInfo = new FileInfo(file.FullName);
                        string filename = fileInfo.Name;
                        FileInfo moveFile = new FileInfo(DirDoneFolder + @"\" + filename);
                        if (moveFile.Exists)
                        {
                            moveFile.Delete();
                        }
                        fileInfo.MoveTo(DirDoneFolder + @"\" + filename);
                    }
                }

                // Reset event
                _busyEvent.Set();
            }
            catch (Exception exception)
            {
                Logger.Error("Something happened during fileWatcher_Created!");
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);

                // I would like to have an e-mail when this happens
                if (ConfigurationManager.AppSettings["SendErrorEmail"].ToUpper() != "TRUE")
                {
                    return;
                }

                MailAddress fromAddress = new MailAddress("505@ntb.no");
                MailAddress toAddress = new MailAddress(ConfigurationManager.AppSettings["EmailReceiver"]);
                MailMessage mailMessage = new MailMessage(fromAddress, toAddress)
                {
                    Subject = "NTBNewsFeedService: Error when creating file",
                    Body = "An error has occured when creating message: " + exception.Message + "\r\n" +
                           "Stack Trace: " + exception.StackTrace
                };

                SmtpClient smtpClient = new SmtpClient { Host = ConfigurationManager.AppSettings["SMTPServer"] };

                smtpClient.Send(mailMessage);
            }

        }
    }
}