﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NTBNewsFeedService.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   Defines the NtbNewsFeedService type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.NewsFeed.Main
{
    using System;
    using System.ServiceProcess;

    // Adding support for Log4Net
    using log4net;

    using Components;

    /// <summary>
    /// The ntb news feed service.
    /// </summary>
    public partial class NtbNewsFeedService : ServiceBase
    {
        /// <summary>
        /// The logger.
        /// </summary>
        protected static readonly ILog Logger = LogManager.GetLogger(typeof(NtbNewsFeedService));

        /// <summary>
        /// The actual service object
        /// </summary>
        protected MainServiceComponent Service = new MainServiceComponent();

        /// <summary>
        /// Initializes a new instance of the <see cref="NtbNewsFeedService"/> class. 
        /// </summary>
        public NtbNewsFeedService()
        {
            log4net.Config.XmlConfigurator.Configure();
            MDC.Set("JOBNAME", "NewsFeedService");


            this.InitializeComponent();
        }

        /// <summary>
        /// When implemented in a derived class, executes when a Start command is sent to the service by the Service Control Manager (SCM) or when the operating system starts (for a service that starts automatically). Specifies actions to take when the service starts.
        /// </summary>
        /// <param name="args">Data passed by the start command.</param>
        protected override void OnStart(string[] args)
        {
            try
            {
                Logger.Info("NewsFeedService starting");
                this.Service.Configure();

                this.Service.Start();

                Logger.Info("NewsFeedService started");
            }
            catch (Exception ex)
            {
                Logger.Fatal("NewsFeedService DID NOT START properly", ex);
            }
        }

        /// <summary>
        /// When implemented in a derived class, executes when a Stop command is sent to the service by the Service Control Manager (SCM). Specifies actions to take when a service stops running.
        /// </summary>
        protected override void OnStop()
        {
            try
            {
                this.Service.Stop();
                Logger.Info("NewsFeedService stopped");
            }
            catch (Exception ex)
            {
                Logger.Fatal("NewsFeedService DID NOT STOP properly", ex);
            }
        }
    }
}
