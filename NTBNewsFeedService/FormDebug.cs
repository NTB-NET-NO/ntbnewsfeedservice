﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FormDebug.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   The form debug.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.NewsFeed.Main
{
    using System;
    using System.Windows.Forms;

    // Adding support for Log4Net library
    using log4net;

    using Components;

    /// <summary>
    /// The form debug.
    /// </summary>
    public partial class FormDebug : Form
    {
        /// <summary>
        /// The actual service object
        /// </summary>
        /// <remarks>Does the actuall work</remarks>
        protected MainServiceComponent Service = null;

        /// <summary>
        /// Logger object
        /// </summary>
        private static ILog _logger;

        /// <summary>
        /// Initializes a new instance of the <see cref="FormDebug"/> class. 
        /// Initializes a new instance of the <see>
        ///                                       <cref>DebugForm</cref>
        ///                                   </see>
        ///     class.
        /// </summary>
        /// <remarks>
        /// Default constructor
        /// </remarks>
        public FormDebug()
        {
            ThreadContext.Properties["JOBNAME"] = "NTBNewsFeedService-Debug";
            log4net.Config.XmlConfigurator.Configure();
            _logger = LogManager.GetLogger(typeof(FormDebug));

            Service = new MainServiceComponent();

            InitializeComponent();
        }

        /// <summary>
        /// The form debug_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void FormDebug_Load(object sender, EventArgs e)
        {
            try
            {
                _logger.Info("NTBNewsFeedService DEBUGMODE starting...");
                _startupConfigTimer.Start();
            }
            catch (Exception ex)
            {
                _logger.Fatal("NTBNewsFeedService DEBUGMODE DID NOT START properly", ex);
            }
        }

        /// <summary>
        /// The form debug_ form closing.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void FormDebug_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                Service.Stop();
                _logger.Info("NTBNewsFeedService DEBUGMODE stopped");
            }
            catch (Exception ex)
            {
                _logger.Fatal("NTBNewsFeedService DEBUGMODE DID NOT STOP properly", ex);
            }
        }

        /// <summary>
        /// The _startup config timer_ tick.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void _startupConfigTimer_Tick(object sender, EventArgs e)
        {
            // Kill the timer
            _startupConfigTimer.Stop();

            // And configure
            try
            {
                Service.Configure();
                Service.Start();
                _logger.Info("NewsFeedService DEBUGMODE started");
            }
            catch (Exception ex)
            {
                _logger.Fatal("NewsFeedService DEBUGMODE DID NOT START properly", ex);
            }
        }
    }
}