﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Program.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   The program.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace NTB.NewsFeed.Main
{
    using System.ServiceProcess;

    /// <summary>
    /// The program.
    /// </summary>
    internal static class ServiceStart
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        private static void Main()
        {
            ServiceBase[] servicesToRun = new ServiceBase[] { new NtbNewsFeedService() };
            ServiceBase.Run(servicesToRun);
        }
    }
}