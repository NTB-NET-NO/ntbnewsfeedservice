﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;

namespace NTB.NewsFeed.Utilities
{
    public static class Mail
    {
        // This class shall be used to send out mail messages when there is an exception

        public static void SendException(Exception exception)
        {
            string fromAddress = ConfigurationManager.AppSettings["SMTPFromAddress"];
            string toAddress = ConfigurationManager.AppSettings["EmailReceiver"];
            string smtpServer = ConfigurationManager.AppSettings["SMTPServer"];
            
            List<string> addresses = new List<string>();

            if (!toAddress.Contains(";"))
            {
                addresses.Add(toAddress);

            }
            else
            {
                string[] toAddresses = toAddress.Split(';');

                addresses.AddRange(toAddresses);
            }

            foreach (MailMessage message in addresses.Select(address => new MailMessage(fromAddress, address)))
            {
                string mailMessage = "Feilmelding fra NewsFeed Service: "
                                     + Environment.NewLine
                                     + exception.Message + Environment.NewLine
                                     + exception.StackTrace + Environment.NewLine;
                    
                if (exception.InnerException != null)
                {
                    mailMessage += "Inner exception: "
                                   + Environment.NewLine
                                   + exception.InnerException.Message + Environment.NewLine
                                   + exception.InnerException.StackTrace + Environment.NewLine;
                }


                message.Subject = "Feilmelding fra NewsFeed Service";
                message.Body = mailMessage;

                SmtpClient client = new SmtpClient(smtpServer);
                client.Send(message);
            }
        }

        public static void SendMessage(string messageText, List<string> emailReceivers )
        {
            // Just in case we have forgotten one check
            if (!emailReceivers.Any())
            {
                return;
            }

            string fromAddress = ConfigurationManager.AppSettings["SMTPFromAddress"];
            
            string smtpServer = ConfigurationManager.AppSettings["SMTPServer"];

            foreach (MailMessage message in emailReceivers.Select(address => new MailMessage(fromAddress, address)))
            {
                string mailMessage = "Melding fra NewsFeed Service: " 
                    + Environment.NewLine
                    + messageText + Environment.NewLine;
                

                message.Subject = "Melding fra NewsFeed Service";
                message.Body = mailMessage;

                SmtpClient client = new SmtpClient(smtpServer);
                client.Send(message);
            }
        }
    }
}
