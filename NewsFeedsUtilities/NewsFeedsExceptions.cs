﻿using System;

namespace NTB.NewsFeed.Utilities
{
    public class NewsFeedsException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see>
        ///                                       <cref>FrontendException</cref>
        ///                                   </see>
        ///     class.
        /// </summary>
        /// <param name="message">The message.</param>
        public NewsFeedsException(string message)
            : base(message)
        { }

        /// <summary>
        /// Initializes a new instance of the <see>
        ///                                       <cref>FrontendException</cref>
        ///                                   </see>
        ///     class.
        /// </summary>
        /// <param name="message">The error message.</param>
        /// <param name="exception">INternal exception.</param>
        public NewsFeedsException(string message, Exception exception)
            : base(message, exception)
        { }

    }
}
