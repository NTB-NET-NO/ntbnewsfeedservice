﻿using System;
using System.Xml.XPath;
using log4net;

namespace NTB.NewsFeed.Utilities
{
    public class XmlUtilities
    {
        /// <summary>
        /// Static logger
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(FileUtilities));

        /// <summary>
        /// Method returning DocumentId from document using Xpath
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public string GetDocumentId(string filename)
        {

            try
            {
                Logger.Debug("Getting Document id from " + filename);
                var xpathDocument = new XPathDocument(filename);

                // I want to check if the document has been produced in the feed before.
                XPathNavigator navigator = xpathDocument.CreateNavigator();
                XPathNodeIterator iterator = navigator.Select("//meta[@name='NTBID']/@content");
                
                // For some reason we could end up with a null value, 
                // so we check the doc-id element + id-string attribute
                if (iterator.Current.Value == null)
                {
                    iterator = navigator.Select("//docdata/doc-id/@id-string");
                }

                // Getting the value
                iterator.MoveNext();

                Logger.Debug("Value found is: " + iterator.Current.Value);

                string docId = iterator.Current.Value.Trim();
                if (docId == string.Empty)
                {
                    docId = "NTB" + DateTime.Now.Year + DateTime.Now.Month.ToString().PadLeft(2) +
                            DateTime.Now.Day.ToString().PadLeft(2) +
                            "T" + DateTime.Now.Hour.ToString().PadLeft(2) + DateTime.Now.Minute.ToString().PadLeft(2) +
                            DateTime.Now.Second.ToString().PadLeft(2);
                }
                return docId;
            }
            catch (Exception exception)
            {
                Logger.Error("An error has occured while getting DocumentId");
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);

                Mail.SendException(exception);

                return string.Empty;
            }
        }

        /// <summary>
        /// Method returning the Document Version from the XML file
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public int GetDocumentVersion(string filename)
        {
            try
            {
                Logger.Debug("Getting Document id from " + filename);
                var xpathDocument = new XPathDocument(filename);

                // I want to check if the document has been produced in the feed before.
                XPathNavigator navigator = xpathDocument.CreateNavigator();
                XPathNodeIterator iterator = navigator.Select("//du-key/@version");

                // Getting the value
                iterator.MoveNext();

                Logger.Debug("Version found is: " + iterator.Current.Value);

                return Convert.ToInt32(iterator.Current.Value);
            }
            catch (Exception exception)
            {
                Logger.Error("An error has occured while getting DocumentId");
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);

                Mail.SendException(exception);

                return 0;
            }
        }
    }
}
