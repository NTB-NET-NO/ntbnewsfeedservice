﻿// -----------------------------------------------------------------------
// <copyright file="JsonConverter.cs" company="NTB">
// NTB
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;
using System.Dynamic;
using System.IO;

namespace NTB.NewsFeed.Utilities
{
    using System;
    using System.Linq;
    using System.Xml;
    
    // Using Log4Net logging library
    using log4net;

    // Using newtonsoft JSON library
    using Newtonsoft.Json;

    using Formatting = Newtonsoft.Json.Formatting;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class JsonConverter : IDisposable
    {
        #region Logging

        /// <summary>
        /// Static logger
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(JsonConverter));

        #endregion

        /// <summary>
        /// The disposed.
        /// </summary>
        private bool disposed;

        /// <summary>
        /// Gets or sets the xml file.
        /// </summary>
        public string XmlFile { get; set; }

        /// <summary>
        /// Gets or sets the done path.
        /// </summary>
        public string DonePath { get; set; }

        /// <summary>
        /// Gets or sets the error path.
        /// </summary>
        public string ErrorPath { get; set; }

        /// <summary>
        /// Gets or sets the feed path.
        /// </summary>
        public string FeedPath { get; set; }

        /// <summary>
        /// Gets or sets the output file name.
        /// </summary>
        public string OutputFileName { get; set; }

        /// <summary>
        /// Gets or sets the Mail Receivers list - used to give information to system
        /// </summary>
        public List<string> MailReceivers { get; set; }

        /// <summary>
        /// The convert file.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string ConvertFile()
        {
            try
            {
                // CHecking if we can access the file
                FileUtilities fileUtilities = new FileUtilities();
                FileInfo fileInfo = new FileInfo(XmlFile);

                fileUtilities.CheckAccessableFile(fileInfo);

                // TODO: See if this code can be done more dynamically and without this Aftenposten Hack
                // To convert an XML node contained in string xml into a JSON string   
                XmlDocument xmlDocument = new XmlDocument();

                Logger.DebugFormat("Loading {0}", XmlFile);
                xmlDocument.Load(XmlFile);
                
                XmlNodeList xmlNodeList = xmlDocument.SelectNodes("//nyheter");

                XmlDocument newXmlDocument = new XmlDocument();
                if (xmlNodeList != null)
                {
                    string xmlPart = xmlNodeList.Cast<XmlNode>().Aggregate("<aftenposten>", (current, xmlNode) => current + xmlNode.OuterXml);

                    xmlPart += "</aftenposten>";
                    newXmlDocument.LoadXml(xmlPart);
                }
                Logger.Info("Serializing the content");
                
                // Check if image is available in the XML
                XmlNodeList xmlImageNodes = xmlDocument.SelectNodes("//bilde");
                
                int images = 0;
                if (xmlImageNodes != null)
                {
                    images = xmlImageNodes.Count;
                }

                Logger.Info("We have " + images + " images in the feed");

                string jsonContent = JsonConvert.SerializeXmlNode(newXmlDocument, Formatting.Indented, true);

                try
                {
                    if (MailReceivers.Count > 0)
                    {
                        Mail.SendMessage(jsonContent, MailReceivers);
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error(exception);
                }

                return jsonContent;


            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);

                return string.Empty;
            }
        }

        /// <summary>
        /// The dispose.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);

            // This object will be cleaned up by the Dispose method. 
            // Therefore, you should call GC.SupressFinalize to 
            // take this object off the finalization queue 
            // and prevent finalization code for this object 
            // from executing a second time.
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// The dispose.
        /// Dispose(bool disposing) executes in two distinct scenarios. 
        /// If disposing equals true, the method has been called directly 
        /// or indirectly by a user's code. Managed and unmanaged resources 
        /// can be disposed. 
        /// If disposing equals false, the method has been called by the 
        /// runtime from inside the finalizer and you should not reference 
        /// other objects. Only unmanaged resources can be disposed. 
        /// </summary>
        /// <param name="disposing">
        /// The disposing.
        /// </param>
        protected virtual void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called. 
            if (!this.disposed)
            {
                // If disposing equals true, dispose all managed 
                // and unmanaged resources. 
                if (disposing)
                {
                    // Dispose managed resources.
                    DonePath = string.Empty;
                    ErrorPath = string.Empty;
                    FeedPath = string.Empty;
                    OutputFileName = string.Empty;
                    XmlFile = string.Empty;
                }

                // Call the appropriate methods to clean up 
                // unmanaged resources here. 
                // If disposing is false, 
                // only the following code is executed.

                // Note disposing has been done.
                disposed = true;
            }
        }
    }
}
