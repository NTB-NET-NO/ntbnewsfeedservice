﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NewsConverter.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   This class shall do the XSLT-converting. And we are to use only XSLT1.0 because the file has gone through everything else
// </summary>
// --------------------------------------------------------------------------------------------------------------------

// Adding support for XML and XSLT

namespace NTB.NewsFeed.Utilities
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.IO;
    using System.Net.Mail;
    using System.Text;
    using System.Threading;
    using System.Xml;
    using System.Xml.XPath;
    using System.Xml.Xsl;

    using log4net;

    /// <summary>
    /// This class shall do the XSLT-converting. And we are to use only XSLT1.0 because the file has gone through everything else
    /// </summary>
    public class NewsConverter : IDisposable
    {
        #region Logging

        /// <summary>
        /// Static logger
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof(NewsConverter));

        #endregion
        /// <summary>
        /// Track whether Dispose has been called. 
        /// </summary>
        private bool _disposed;

        /// <summary>
        /// Gets or sets the xslt file.
        /// </summary>
        public string XsltFile { get; set; }

        /// <summary>
        /// Gets or sets the xslt combine file.
        /// </summary>
        public string XsltCombineFile { get; set; }

        /// <summary>
        /// Gets or sets the xml file.
        /// </summary>
        public string XmlFile { get; set; }

        /// <summary>
        /// Gets or sets the encoding.
        /// </summary>
        public string FileEncoding { get; set; }

        /// <summary>
        /// Gets or sets the out path.
        /// </summary>
        public string OutPath { get; set; }

        /// <summary>
        /// Gets or sets the done path.
        /// </summary>
        public string DonePath { get; set; }

        /// <summary>
        /// Gets or sets the error path.
        /// </summary>
        public string ErrorPath { get; set; }

        /// <summary>
        /// Gets or sets the feed path.
        /// </summary>
        public string FeedPath { get; set; }

        /// <summary>
        /// Gets or sets the output file name.
        /// </summary>
        public string OutputFileName { get; set; }

        /// <summary>
        /// Gets or sets the max files.
        /// </summary>
        public int MaxFiles { get; set; }

        /// <summary>
        /// Gets or sets the x path expression.
        /// </summary>
        public string XPathFilter { get; set; }

        /// <summary>
        /// Gets or sets the documentId
        /// </summary>
        public string DocumentId { get; set; }

        /// <summary>
        /// Gets or sets the document version
        /// </summary>
        public int DocumentVersion { get; set; }
        

        /// <summary>
        /// CombineFiles takes a list of files and combines them using XSLT
        /// </summary>
        /// <param name="files">List containing a filenames to be combined</param>
        public void CombineFiles(List<FileInfo> files)
        {
            // TODO: Consider breaking this method into a smaller chunck. 
            // Creating a string where we can store the combined file content
            var doc = new XmlDocument();

            var xmlRoot = doc.CreateElement("NewsML");
            var xmlCatalog = doc.CreateElement("Catalog");
            xmlCatalog.SetAttribute("Href", "http://cv.ntb.no/catalog.xml");
            xmlRoot.AppendChild(xmlCatalog);

            var xmlEnvelope = doc.CreateElement("NewsEnvelope");
            var xmlDateTime = doc.CreateElement("DateAndTime");

            /*
             * string sMonth = DateTime.Now.Month.ToString().PadLeft(2, "0");

string sDay = DateTime.Now.Day.ToString().PadLeft(2, "0");
             */
            var cdt = DateTime.Now;
            var stringDateTime = cdt.Year + cdt.Month.ToString().PadLeft(2, '0') +
                                    cdt.Day.ToString().PadLeft(2, '0') + "T" + cdt.Hour +
                                    cdt.Minute + cdt.Second;
            var textNode = doc.CreateTextNode(stringDateTime);
            xmlDateTime.AppendChild(textNode);
            
            // TODO: Consider adding a template so it's not MSN specific
            var xmlNewsService = doc.CreateElement("NewsService");
            xmlNewsService.SetAttribute("FormalName", "Thumbnail");
            xmlNewsService.SetAttribute("link-url", "http://www.ntb.no");
            textNode = doc.CreateTextNode("http://www.ntb.no/images/logo_ntb.gif");
            xmlNewsService.AppendChild(textNode);
            xmlEnvelope.AppendChild(xmlDateTime);
            xmlEnvelope.AppendChild(xmlNewsService);
            xmlRoot.AppendChild(xmlEnvelope);

            var listDuid = new List<string>();
            bool boolCreateFile = false;
            foreach (var file in files)
            {
                // This is being done elsewhere now, so I can remove this part of the code (I believe)
                var xpathDocument = new XPathDocument(file.FullName);

                // I want to check if the document has been produced in the feed before.
                var navigator = xpathDocument.CreateNavigator();
                var iterator = navigator.Select("//NewsComponent/@Duid");

                // We know that we will get a hit, because that is what we do...
                iterator.MoveNext();
                var stringDuid = iterator.Current.Value;
                
                Logger.Info("Checking if we have " + stringDuid + " in the feed already");

                if (!listDuid.Contains(stringDuid))
                {
                    listDuid.Add(stringDuid);
                    boolCreateFile = true;
    
                    try
                    {
                        var xslCompiledTransform = new XslCompiledTransform();

                        var settings = new XmlWriterSettings
                            {
                                OmitXmlDeclaration = true,
                                Indent = true,
                                Encoding = Encoding.UTF8
                            };

                        Logger.Debug("Loading: " + Path.GetFileName(XsltCombineFile));
                        xslCompiledTransform.Load(XsltCombineFile);

                        var memoryStream = new MemoryStream();
                        var xmlWriter = XmlWriter.Create(memoryStream, settings);
                        xslCompiledTransform.Transform(xpathDocument, null, xmlWriter);

                        memoryStream.Position = 0;

                        var xmlTextReader = new XmlTextReader(memoryStream)
                            {
                                WhitespaceHandling = WhitespaceHandling.None
                            };
                        
                        var newDoc = new XmlDocument();

                        Logger.InfoFormat("Reading {0}", file.FullName);
                        newDoc.Load(xmlTextReader);

                        var xmlFragment = doc.CreateDocumentFragment();
                        xmlFragment.InnerXml = newDoc.InnerXml;

                        xmlRoot.AppendChild(xmlFragment);
                    }
                    catch (Exception exception)
                    {
                        Logger.Error(exception.Message);
                        Logger.Error(exception.StackTrace);
                    }    
                }
                else
                {
                    // We can delete the file, and so we could have more data in the feed
                    Logger.Info("We are deleting the file, as we already have a newer version in the feed");
                    file.Delete();
                }

                // now we shall write the file
                if (boolCreateFile)
                {
                    if (OutputFileName == string.Empty)
                    {
                        throw new Exception("Cannot continue. Feedname is empty");
                    }

                    // Creating the feedFile
                    string feedFile = FeedPath + @"\" + OutputFileName;

                    Logger.Debug("To check if " + feedFile + " exists");
                    
                    // Deleting feed file
                    DeleteFeedFile(feedFile);

                    Logger.Debug("Creating XmlTextWriter for " + feedFile);

                    const bool saveXml = false;

                    // Saving feed file
                    SaveFeedFile(saveXml, feedFile, doc, xmlRoot);
                }
            }

            Logger.Debug("Setting XmlDocument object to null");
        }

        /// <summary>
        /// Method saving the feed file
        /// </summary>
        /// <param name="saveXml">boolean value used to make sure we are able to save the file</param>
        /// <param name="feedFile">full name with path for the feed file</param>
        /// <param name="doc">XML Structure for the feed file</param>
        /// <param name="xmlRoot">the Root XML Element</param>
        private static void SaveFeedFile(bool saveXml, string feedFile, XmlDocument doc, XmlElement xmlRoot)
        {
            var xmlCounter = 0;
            while (!saveXml)
            {
                try
                {
                    var xmlTextWriter = new XmlTextWriter(feedFile, null);

                    Logger.Debug("Formatting with indent");
                    xmlTextWriter.Formatting = Formatting.Indented;

                    Logger.Debug("Appending the data");
                    doc.AppendChild(xmlRoot);

                    Logger.Info("Saving document: " + feedFile);
                    doc.Save(xmlTextWriter);

                    Logger.Debug("Closing XmlTextWriter");
                    xmlTextWriter.Close();

                    Logger.Debug("Setting XmlTextWriter object to null");
                    saveXml = true;
                }
                catch (Exception exception)
                {
                    Logger.Debug(exception.Message);
                    if (xmlCounter >= 10)
                    {
                        throw new NewsFeedsException("Cannot Save the feedfile");
                    }

                    Thread.Sleep(2000);
                    xmlCounter++;
                }
            }
        }
        /// <summary>
        /// Method deleting feed file before we save a new version
        /// </summary>
        /// <param name="feedFile">string holding the full name of the file</param>
        private static void DeleteFeedFile(string feedFile)
        {
            if (!File.Exists(feedFile)) return;

            bool deleteOk = false;
            int counter = 0;
            while (!deleteOk)
            {
                try
                {
                    Logger.Info("Delete " + feedFile);
                    File.Delete(feedFile);
                    deleteOk = true;
                }
                catch (Exception exception)
                {
                    Logger.Debug(exception.Message);
                    if (counter >= 10)
                    {
                        throw new NewsFeedsException("Cannot delete the feedfile");
                    }
                    
                    // Waiting two seconds before we try again
                    Thread.Sleep(2000);
                    counter++;
                    
                }
            }
        }

        /// <summary>
        /// ConvertFile converts the files to NewsML+NITF and puts them in the Cache-folder
        /// </summary>
        public void ConvertFile()
        {
            var fileUtilities = new FileUtilities();

            try
            {
                Logger.Info("In ConvertFile to convert " + Path.GetFileName(this.XmlFile));

                Logger.Debug("Creating myXPathDoc - the document");

                // Setting myXpathDoc XPathDocument variable
                XPathDocument xpathDocument = null;

                // Creating an Accessable bool variables to be used to test for accessable files
                var accessable = false;

                // Creating a counter variable. If this becomes larger than 10, we shall throw an exception
                var accessableCounter = 0;

                // Doing a while-loop until the file becomes accessable
                while (!accessable)
                {
                    try
                    {
                        xpathDocument = new XPathDocument(XmlFile);
                        accessable = true;
                    }
                    catch (Exception ex)
                    {
                        if (accessableCounter < 10)
                        {
                            accessableCounter++;
                            Thread.Sleep(1000);
                        }
                        else
                        {
                            Logger.Error(ex.Message);
                            Logger.Error(ex.StackTrace);
                            throw new NewsFeedsException("Cannot read " + XmlFile);
                        }
                    }
                }

                var documentTitle = GetDocumentTitle(xpathDocument);

                var hasPictures = CheckMessageForPictures(xpathDocument);
                var hasPictureReferences = CheckMessageForPictureReference(xpathDocument, hasPictures);

                // now we check if the file contains the different parts
                var message = CreateMailMessage(hasPictures, hasPictureReferences, documentTitle);

                if (message != string.Empty) {
                    try
                    {
                        // We shall only send e-mails if the list is not empty
                        if (this.MailReceivers.Count > 0)
                        {
                            Mail.SendMessage(message, this.MailReceivers);
                        }
                    }
                    catch (Exception exception)
                    {
                        Logger.Error(exception.Message);
                        Logger.Error(exception.StackTrace);
                    }
                }


                Logger.Debug("Creating XSLT object");
                var xslCompiledTransform = new XslCompiledTransform();

                // Checking if XSLT-file exists
                var fileInfo = new FileInfo(this.XsltFile);

                if (!fileInfo.Exists)
                {
                    return;
                }
                Logger.Debug("Loading: " + Path.GetFileName(this.XsltFile));
                xslCompiledTransform.Load(this.XsltFile);

                Logger.Debug("Cloning settings");
                var set = xslCompiledTransform.OutputSettings.Clone();
                set.Indent = true;

                Logger.Debug("Setting outpath to: " + Path.GetFileName(this.XmlFile));
                var orgName = Path.GetFileName(this.XmlFile);

                // string outName = Path.GetFileName(XmlFile);
                var outName = this.DocumentId + @"-" + DocumentVersion + ".xml";

                if (outName.Length > 50)
                {
                    outName = "NTB" + DateTime.Now.Year + DateTime.Now.Month.ToString().PadLeft(2) +
                            DateTime.Now.Day.ToString().PadLeft(2) +
                            "T" + DateTime.Now.Hour.ToString().PadLeft(2) + DateTime.Now.Minute.ToString().PadLeft(2) +
                            DateTime.Now.Second.ToString().PadLeft(2) + ".xml";
                }
                Logger.Info("Transforming " + outName);
                Logger.Info("Writing: " + this.OutPath + @"\" + outName);


                using (var xmlWriter = XmlWriter.Create(this.OutPath + @"\" + outName, set))
                {
                    try
                    {
                        xslCompiledTransform.Transform(xpathDocument, xmlWriter);
                    }
                    catch (Exception exception)
                    {
                        Logger.Error("There was an error saving the document");
                        Logger.Error("We tried to write: " + this.OutPath + @"\" + outName);

                        Logger.Error(exception.Message);
                        Logger.Error(exception.StackTrace);

                        if (exception.InnerException != null)
                        {
                            Logger.Error(exception.InnerException.Message);
                            Logger.Error(exception.InnerException.StackTrace);

                        }

                        // now we need to move the error file
                        fileUtilities.MoveFileToFolder(this.XmlFile, this.ErrorPath);
                    }
                }

                // now we shall move the transformed document
                Logger.Info("Moving file to: " + this.DonePath + @"\" + orgName);

                // Moving file to done path
                fileUtilities.MoveFileToFolder(this.XmlFile, this.DonePath);
            }
            catch (IOException ioexception)
            {
                Logger.Error("Something happened when working on file " + this.XmlFile);
                Logger.Error("Moving file to " + this.ErrorPath);
                fileUtilities.MoveFileToFolder(this.XmlFile, this.ErrorPath);
                
                Logger.Error(ioexception.Message);
                Logger.Error(ioexception.StackTrace);
            }
            catch (Exception exception)
            {
                Logger.Error("An exception occured in NewsConverter!");
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);

                // I would like to have an e-mail when this happens
                if (ConfigurationManager.AppSettings["SendErrorEmail"].ToUpper() != "TRUE")
                {
                    return;
                }

                var fromAddress = new MailAddress("505@ntb.no");
                var toAddress = new MailAddress(ConfigurationManager.AppSettings["EmailReceiver"]);
                var mailMessage = new MailMessage(fromAddress, toAddress)
                    {
                        Subject =
                            "NTBNewsFeedService: Error when creating file",
                        Body =
                            "An error has occured when creating message: "
                            + exception.Message + "\r\n"
                            + "Stack Trace: "
                            + exception.StackTrace + "\r\n" 
                            + "File tried to be converted or saved was: " + "\r\n" 
                            + this.XmlFile
                    };

                var smtpClient = new SmtpClient { Host = ConfigurationManager.AppSettings["SMTPServer"] };

                smtpClient.Send(mailMessage);

                Logger.Error("Something happened when working on file " + this.XmlFile);
                Logger.Error("Moving file to " + this.ErrorPath);
                fileUtilities.MoveFileToFolder(this.XmlFile, this.ErrorPath);
            }
        }

        private static string CreateMailMessage(bool hasPictures, bool hasPictureReferences, string documentTitle)
        {
            if (hasPictures && hasPictureReferences == false)
            {
                // We must warn 
                if (documentTitle == string.Empty)
                {
                    documentTitle = "Denne meldingen";
                }
                var mailMessage = documentTitle +
                                     " skulle ha inneholdt bildereferanse, men vi kan ikke finne noen bildereferanser i meldingen.\r\n" +
                                     "Grunnen kan være at du har valgt direktebilder (illustrasjonsbilder) som blir fjernet av Notabene fra meldinger som \r\n" +
                                     "ikke er direktemeldinger. \r\n" +
                                     "\r\n"+
                                     "Sjekkliste:\r\n"+
                                     "--------------\r\n"+
                                     "* Sjekk om du har lagt på bilder på meldingen" +
                                     "* Sjekk om du har brukt et direktebilde og at meldingen ikke er en direktemelding.\r\n" +
                                     "\r\n"+
                                     "--------------\r\n" +
                                     "\r\n"+
                                     documentTitle + " er lagt inn i feeden. \r\n" +
                                     "Send meldingen på nytt. Husk at meldingen må nyes...\r\n" +
                                     "\r\n" +
                                     "Denne meldingen er generert automatisk av NewsFeedService. \r\n";

                return mailMessage;
            }

            return string.Empty;
        }

        private static string GetDocumentTitle(XPathDocument xPathDocument)
        {
            if (xPathDocument == null)
            {
                throw new ArgumentNullException("xPathDocument");
            }
            var node = xPathDocument.CreateNavigator().SelectSingleNode("/nitf/head/title");

            return node != null ? node.Value : string.Empty;
        }

        private static bool CheckMessageForPictureReference(XPathDocument xPathDocument, bool hasPictures)
        {
            if (xPathDocument == null)
            {
                throw new ArgumentNullException("xPathDocument");
            }

            if (!hasPictures)
            {
                return false;
            }
                // now we must check if we have a media element
            var node = xPathDocument.CreateNavigator().SelectSingleNode("/nitf/body/body.content/media");

            return node != null;
        }

        private static bool CheckMessageForPictures(XPathDocument xPathDocument)
        {
            if (xPathDocument == null)
            {
                throw new ArgumentNullException("xPathDocument");
            }

            // Checking for pictures
            var node =
                xPathDocument.CreateNavigator()
                             .SelectSingleNode("/nitf/head/meta[@name='NTBBilderAntall']/@content");

            if (node == null)
            {
                return false;
            }

            return Convert.ToInt32(node.Value) > 0;
        }

        /// <summary>
        /// The dispose.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);

            // This object will be cleaned up by the Dispose method. 
            // Therefore, you should call GC.SupressFinalize to 
            // take this object off the finalization queue 
            // and prevent finalization code for this object 
            // from executing a second time.
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// The dispose.
        /// Dispose(bool disposing) executes in two distinct scenarios. 
        /// If disposing equals true, the method has been called directly 
        /// or indirectly by a user's code. Managed and unmanaged resources 
        /// can be disposed. 
        /// If disposing equals false, the method has been called by the 
        /// runtime from inside the finalizer and you should not reference 
        /// other objects. Only unmanaged resources can be disposed. 
        /// </summary>
        /// <param name="disposing">
        /// The disposing.
        /// </param>
        protected virtual void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called. 
            if (this._disposed)
            {
                return;
            }

            // If disposing equals true, dispose all managed 
            // and unmanaged resources. 
            if (disposing)
            {
                // Dispose managed resources.
                this.DonePath = string.Empty;
                this.ErrorPath = string.Empty;
                this.FeedPath = string.Empty;
                this.MaxFiles = 0;
                this.OutputFileName = string.Empty;
                this.OutPath = string.Empty;
                this.XsltCombineFile = string.Empty;
                this.XsltFile = string.Empty;
                this.XmlFile = string.Empty;
                this.XPathFilter = string.Empty;
                this.FileEncoding = string.Empty;
            }

            // Call the appropriate methods to clean up 
            // unmanaged resources here. 
            // If disposing is false, 
            // only the following code is executed.

            // Note disposing has been done.
            this._disposed = true;
        }

        public List<string> MailReceivers { get; set; }
    }
}