﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using log4net;

namespace NTB.NewsFeed.Utilities
{
    public class FileUtilities
    {
        /// <summary>
        ///     Static logger
        /// </summary>
        private static readonly ILog Logger = LogManager.GetLogger(typeof (FileUtilities));

        /// <summary>
        ///     The check accessable file.
        /// </summary>
        /// <param name="fi">
        ///     The fi.
        /// </param>
        /// <exception cref="Exception">
        ///     Throws an exception if we cannot access the file
        /// </exception>
        public void CheckAccessableFile(FileInfo fi)
        {
            int accessCounter = 0;
            while (IsFileAccessable(fi.FullName) == false)
            {
                Thread.Sleep(1500);
                accessCounter++;
                if (accessCounter == 5)
                {
                    throw new Exception("Cannot access file. Is being used by some other instance!");
                }
            }
        }

        private bool IsFileAccessable(string filename)
        {
            try
            {
                using (FileStream fs = File.Open(filename, FileMode.Open, FileAccess.Read, FileShare.None))
                {
                    fs.Close();
                }
            }
            catch (IOException ioexception)
            {
                Logger.Error(ioexception.Message);
                Logger.Error(ioexception.StackTrace);
                return false;
            }

            return true;
        }

        /// <summary>
        ///     This method creates the list of files to be deleted. The deletion will happen in the delete files method.
        /// </summary>
        /// <param name="numberOfFilesAllowed">integer telling how many files to keep</param>
        /// <param name="allFiles">List of files we have in the directory</param>
        /// <param name="cacheFiles">List of files in the cache</param>
        /// <returns>List of files containing the names of the files we are to delete </returns>
        public List<string> FilesToDelete(int numberOfFilesAllowed, List<FileInfo> allFiles, List<FileInfo> cacheFiles)
        {
            return allFiles.Select(x => x.FullName).Except(cacheFiles.Select(y => y.FullName)).ToList();
        }

        public List<FileInfo> GetCacheFiles(string directoryName)
        {
            var directoryInfo = new DirectoryInfo(directoryName);
            return directoryInfo.GetFiles("*.xml").ToList();
        }

        /// <summary>
        ///     Method to find the article in the cache file based on the filename
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="cacheFiles"></param>
        /// <returns></returns>
        public bool FindFileInCache(string filename, List<FileInfo> cacheFiles)
        {
            Logger.Debug("Filename to find:" + filename);
            string file = (from cache in cacheFiles
                           where cache.Name == filename
                           select cache.Name).ToString();

            return file != "";
        }

        /// <summary>
        ///     Method to find file in cache based on the article document id
        /// </summary>
        /// <param name="documentId">string containing the document id</param>
        /// <param name="cacheFiles">list of files in the cache directory</param>
        /// <returns>true if we've found the file, false otherwise</returns>
        public bool FindFileInCacheByDocumentId(string documentId, List<FileInfo> cacheFiles)
        {
            // if the directory is empty, we shall return false. Nothing to find
            if (!cacheFiles.Any())
            {
                return false;
            }

            Logger.Debug("Document Id: " + documentId);
            var file = (from cache in cacheFiles
                           where cache.Name.Contains(documentId)
                           select cache.Name).ToString();

            // Returning either true or false based on the outcome of the test above
            return file != "";
        }

        /// <summary>
        ///     Method to find if we have a new version of the file already in the cache folder
        /// </summary>
        /// <param name="versionNumber">integer containing the document/article version</param>
        /// <param name="documentId">string containing the document id</param>
        /// <param name="cacheFiles">list of files in the cache directory</param>
        /// <returns>true if we've found the file, false otherwise</returns>
        public bool FindFileVersionByVersionNumber(int versionNumber, string documentId, List<FileInfo> cacheFiles)
        {
            // Making sure we don't crash anything.
            if (!cacheFiles.Any())
            {
                return false;
            }

            Logger.Debug("Checking if " + cacheFiles[0].FullName);
            Logger.Debug("Version Number: " + versionNumber);
            try
            {
                if (cacheFiles.Count == 0)
                {
                    return false;
                }
                
                List<string> files = (from cache in cacheFiles
                                      where cache.Name.Contains(documentId)
                                      select cache.Name).ToList();
                if (files.Count > 1 || files.Count == 0)
                {
                    return false;
                }
                string file = files.Single();
                
                if (string.IsNullOrEmpty(file))
                {
                    return false;
                }

                string[] filenameParts = file.Split('-');

                if (!filenameParts.Any())
                {
                    return false;
                }

                return filenameParts[1].Replace(".xml", "") == versionNumber.ToString();
            }
            catch (Exception exception)
            {
                // This actually means that there was no hit
                Logger.Info(exception);
                return false;
            }
        }

        /// <summary>
        ///     Public method to delete a file from the cache
        /// </summary>
        /// <param name="filename">File to be deleted</param>
        public void DeleteCacheFile(FileInfo filename)
        {
            if (filename.Exists)
            {
                filename.Delete();
            }
        }

        /// <summary>
        ///     Deleting files in customer cache folder
        /// </summary>
        /// <param name="numberOfFilesAllowed">number containing the total number of files allowed for the customer</param>
        /// <param name="cacheFiles">Files in cacheFiles</param>
        /// <param name="allFiles">All files in folder</param>
        public void DeleteFiles(int numberOfFilesAllowed, List<FileInfo> allFiles, List<FileInfo> cacheFiles)
        {
            Logger.Info("Deleting files from Cache folder");

            Logger.Debug("Total files: " + allFiles.Count);
            Logger.Debug("Cache files: " + cacheFiles.Count);

            List<string> deletingFiles =
                allFiles.Select(x => x.FullName).Except(cacheFiles.Select(y => y.FullName)).ToList();

            Logger.Debug("Files to delete: " + deletingFiles.Count);

            foreach (string file in deletingFiles)
            {
                Logger.Debug("file to delete: " + file);
                var fi = new FileInfo(file);
                fi.Delete();
            }
        }

        public string CreateCacheFileName(string documentId, int documentVersion)
        {
            return documentId + "-" + documentVersion + ".xml";
        }

        public string CreatePreviousCacheFileName(string documentId, int documentVersion)
        {
            int docVersion = documentVersion - 1;

            if (docVersion == 0)
            {
                return string.Empty;
            }
            return documentId + "-" + docVersion + ".xml";
        }

        public void MoveFileToFolder(string document, string movePath)
        {
            try
            {
                string newOutName = Path.GetFileName(document);
                if (File.Exists(movePath + @"\" + newOutName))
                {
                    File.Delete(movePath + @"\" + newOutName);
                }

                // If something failed, we move the file to the error folder
                File.Move(document, movePath + @"\" + newOutName);

                if (File.Exists(document))
                {
                    Logger.Debug("Even after moving, the file exists. we will delete!");
                    File.Delete(document);
                }
                else
                {
                    Logger.Debug("File has been moved to : " + movePath + @"\" + newOutName);
                }
            }
            catch (IOException exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);

                if (exception.InnerException == null) return;
                Logger.Error(exception.InnerException.Message);
                Logger.Error(exception.InnerException.StackTrace);
            }
            catch (Exception exception)
            {
                Logger.Error(exception.Message);
                Logger.Error(exception.StackTrace);

                if (exception.InnerException == null) return;
                Logger.Error(exception.InnerException.Message);
                Logger.Error(exception.InnerException.StackTrace);
            }
        }
    }
}